 Sponsors :
  - DipSO
    - Direction pour la Science Ouverte
    - https://www6.inrae.fr/dipso
  - INRAE
    - Institut National de recherche pour l'agriculture, l'alimentation et l'environnement
    - https://www.inrae.fr


Soutiens : 
  - CALMIP
    - Mésocentre CALMIP - UMS 3667 CNRS/Université de Toulouse
    - https://www.calmip.univ-toulouse.fr
  - CCIPL
    - Centre Calcul Intensif Pays de la Loire
    - https://ccipl.univ-nantes.fr
  - DipSO
      - Direction pour la Science Ouverte
      - https://www6.inrae.fr/dipso
  - ECN
    - École Centrale de Nantes
    - https://www.ec-nantes.fr
  - ICI
    - High Performance Computing Institute
    - https://supercomputing.ec-nantes.fr
  - INRAE
    - Institut National de recherche pour l'agriculture, l'alimentation et l'environnement
    - https://www.inrae.fr
  - INSERM
    - Institut national de la santé et de la recherche médicale
    - https://www.inserm.fr
  - Institut Agro
    - Institut national d'enseignement supérieur pour l'agriculture, l'alimentation et l'environnement, Agrocampus Ouest & Montpellier SupAgro
    - https://www.institut-agro.fr
  - ISDM
    - Institut de Science des Données de Montpellier
    - https://isdm.umontpellier.fr
  - GENCI
    - Grand Équipement National de Calcul Intensif
    - https://www.genci.fr
  - GRICAD
    - Grenoble Alpes Recherche - Infrastructure de Calcul Intensif et de Données
    - https://gricad.univ-grenoble-alpes.fr
  - Groupe Calcul
    - https://calcul.math.cnrs.fr
  - Meso@LR
    - Mésocentre Occitanie-EST
    - https://meso-lr.umontpellier.fr
  - MesoPSL
    - Research University, mésocentre Paris Sciences et Lettres
    - http://www.mesopsl.fr
  - ROMEO
    - Centre de Calcul Régional ROMEO
    - https://romeo.univ-reims.fr

 
