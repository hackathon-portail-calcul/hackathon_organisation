# PCSC kick-off Brainstorm Synthèse
28 mai 2020 - 21h

Curration : https://etherpad.in2p3.fr/p/pcsc\_brainstorming\_202005282200

**9h, le 25 mai 2020**
- Tovo Rabemanantsoa, INRAE
- Alexandre Dehne Garcia, INRAE
- Olivier Porte, CNRS
- Olivier Langella, CNRS
- Geneviève Romier, CC-IN2P3, France Grilles
- Loic HOUDE,INRAE
- Christophe Moisy, INRAE
- Jean-François Rey, BioSP @ INRAE
- Cyril Jousse, Univ Clermont Auvergne
- Jacques Lagnel, INRAE, Avignon GAFL
- Cédric Goby 
- Vincent Nègre
    
**11h, le 25 mai 2020**
- Tovo Rabemanantsoa, INRAE
- Alexandre Dehne Garcia, INRAE
- David Benaben, INRAE
- Patrick Chabrier, INRAE
- Sophie Aubin, INRAE
- Estelle Ancelet, INRAE
- Pierre ADENOT INRAE
- Stéphane Paris INRAE
- Emmanuel BRAUX, IMT Atlantique
- Pierre-Emmanuel GUÉRIN, École Centrale de Nantes
- Sophie Desset

**14h, le 25 mai 2020**
- Tovo Rabemanantsoa, INRAE
- Alexandre Dehne Garcia, INRAE
- Hélène Raynal , INRAE
- Oana Vigy, CNRS
- Malika Nassif, INRAE
- Emilie Lerigoleur, CNRS
- Raphaël Flores, INRAE
- Bertrand Pitollat, CIRAD
- Daniel Salas, Inserm
- Richard Randriatoamanana, CNRS
- Jan Drouaud
- Pierre Gay, MCIA
- Julien Pergaud CNRS/Univ Bourgogne
- Caroline Sophie Donati, AMU
- Zenaida Tucsnak , CNRS
- Thierry Labbe, INRAE
- Sylvain Maurin, CNRS Institut des Scs Cognitives
- Nadia Ponts
- Patrice Langlois, CNRS, 

**17h, le 25 mai 2020**
- Tovo Rabemanantsoa, INRAE
- Alexandre Dehne Garcia, INRAE
- Gilles Mathieu, INSERM
- Fred de Lamotte, INRAe
- Arnaud Jean-Charles, CNRS
- Sumaira JAVAID
- Emmanuelle Morin, INRAe
- Paul Rinaudo, Polytechnique
- Anne Laurent, Université de Montpellier
- Sébastien Cat, DSI INRAE
- Mikael Loaec, INRAE


**26 mai 2020 - 9h\_13h**
- Mikael Loaec
- Christophe Moisy
- Cyril Jousse
- Nadia Ponts
- Fred de Lamotte

**26 mai 2020 - 13h\_19h**
- Cédric Goby (IP INRAE, data, informaticien)
- David Benaben (PF CBIB/IFP INRAE, adminsys)

**27 mai 2020 - 9h\_13h**
- Olivier Langella (Dev JS/UI)
- Mikael Loaec
- Gilles Mathieu (Réflexions, use-cases, besoins)
- Raphaël Flores (CI, processus d'alimentation en données par les plateformes, ...)
- Sophie Aubin (Définitions/Vocabulaire pour la partie Documentation et schéma de la plateforme)
- Jacques Lagnel


**27 mai 2020 - 13h\_19h**
- Jacques Lagnel
- Jérémy Verrier (remplir les données sur les centres, rédaction de doc accompagnante pour le chercheur: premiers pas) 
- Estelle Ancelet (faire un retour d'expérience sur leur recherche de solutions, test grandeur nature sur cas concret, définitions des critères, relire de la doc, gitlab + CI, dev HUGO)
- Patrick Chabrier (définitions critères, vérification que une personne de niveau intermédiaire comprenne la doc et interface)
- Pierre Gay (format de fichier)
- Richard Randriatoamanana
- Cyril Jousse (apporter des uses cases, retour d'expérience sur ce qui se fait dans metabo-hub = conception de cahier des charge, benchmarking d'outils, certifications, faire des demandes pour avoir des RH ou $)

**28 mai 2020 - 9h\_12h**
- Mikael Loaec (adminsys)
- Willy Bienvenut (bioinfo Moulon) en recherche de ressources, IFB ne correspond pas
- Emilie Lerigoleur (géo/env geomatique Toulouse, 

   *     réseaux des observatoires hommes-milieux LabEx DRIIHM (CNRS-INEE), intéressées par le cycle de vie de la donnée, 
   *      partage de la données, science ouverte, FAIRisation des données, entrepôts de données, stockage/calcul/archivage, brainstorming sur e-infra)
- Emmanuel Braux (IMT Atlantique Brest, gère un PF openstack, DSI, en accompagnement aux chercheurs sur les outils d'enseignement -> cloud, docbook    - Florian Trincal (DSI INRAE, openstack)
- Nadia Ponts (INRAE bio épigénomique / bioinfo Bx, end-user, analyse big-data)

**28 mai 2020 - 14h\_18h**
- Arnaud JEAN-CHARLES (chargé de projet web/donné Labex DRIIHM, co-porteur du projet SoDRIIHM=PF pour faciliter la gestion de la donnée et FAIRisation, centralisation de l'information utiles aux chercheurs), intéressé par 1) les ontologies + schéma de données et 2) API
- Paul Rénaudot : Engie, Etudiant X, Comment donner une infra aux chercheurs une infra afin que les chercheurs n'aient pas à implémenter leurs propres infra)
- Daniel Salas, INSERM, intéressé par le chabot : aide au user, interface, code et fonctionnement
- David Benaben (cbib)
- Pascal Voury (GENCI), intéressé par les critères des infra 

## Autres portails : 

Portail XSEDE:
\url{https://www.xsede.org/}
\url{https://portal.xsede.org/}

Portail EOSC :
\url{https://marketplace.eosc-portal.eu}
   \url{https://marketplace.eosc-portal.eu/services/c/compute?q=\&service\_id=\&sort=\_score}

    
\url{https://cat.opidor.fr/index.php/Cat\_OPIDoR,\_wiki\_des\_services\_d%C3%A9di%C3%A9s\_aux\_donn%C3%A9es\_de\_la\_recherche}
\url{https://fairsharing.org/}



## FeedBack orga com/flyer/brainstorming

    - difficulté à se projeter dans le hackathon : bcp de monde, nouveaux formats, temps limité
        - le kick-off ne remettais pas assez le contexte et la problématique, trop technique, trop fonctionnel 
    - comment réussir à contribuer ? quelle est l'orga
    - suis-je assez compétent pour aider
    - kick-off trop rapide sur certains aspects : 
        - périmètre inter-instituts, France ? Europe ? Entreprises ?
        - périmètre : que calcul ? juste du matériel ? des services ?
        - architecture de l'outil : trop rapide
        - est-ce un annuaire qui moissonne d'autres annuaires ? une base de donnée ?
    
- orga pas assez claire

   - orga souple mais quelle est la couverture des participants sur les instituts ? Sont-il convaincus ?         - quelle soutien institutionnel, quelle pérennité ? (très important) avoir de bonne fondations
    - kick-off : format court apprécié
    - kick-off : bien, plein de user d'horizons différents
    - kick-off : bien pour donner le top-départ
    - kick-off : court si beaucoup de personne
    - brainstorming bien pour trouver les idées de chacun
- avoir une boite à idées centralisée (channel mattermost + fichier md dans le git)
- mettre en place un kaban board?
- boite à idée pour l'OAD

   - tout le monde pouvait parlé durant le kick-off : bien
   - prez clair
   - la période brainstorming est bien pour pouvoir savoir ce que chacun peut-apporter = rassurant
   - première impression sur le gitlab : toufu, lecture difficile au milieu de tous ces liens mais kick-off aide à mieux comprendre le tout
   - entrée en douceur dans le projet est appréciée : passage d'observateur à acteur assez facile
   - motivation : venu par connaissance des organisateurs, bon projet, super flyer
   - kick-off réparti en plusieurs sessions =  très bien, souple
       - ambiance décontractée, tour de table permet à tout le monde de s'exprimer facilement 
       - prez claire
       - que va sortir du hackathon, du brainstorming 
       - difficile de savoir ce qui va en sortir * Maîtrise commune des outils (W)
* Mettre à dispo les slides
* Intérêts convergents
* curiosité sur le mode de fonctionnement (+3)
* orga difficile
* Répartition des groupes (W)
 
 
    
## Warnings et remarques globaux
  - attention on part à 50, on finie à 3 et la pérennité en prend un coup
  -  attention l'obsolescence des données !!!!
  - ok pour un portail mais faut-il que les end-users trouvent le portail : ne pas négliger la com
  - accompagner : cours
  - projet a une réelle utilité, même une nécessité
  
- risque vis-à-vis des infras qui ne veulent pas être comparer à d'autres 

   -> éviter le côté commercial, les infras ne sont pas des objets de consommation courante, éviter les notations) 
- orga et techno plait beaucoup
- outil chouette car l'outil permet à l'utilisateur de fouiller le ressources et même de découvrir des solutions et besoins dont il aurait besoin
- OAD ++
- montrer aux utilisateurs le taux de charge en calcul des infras n'a pas de sens pour le calcul, ni pour le stockage
- il faut commencer petit 
- se faire certifier pour convaincre, apporter des garanties 
- besoin de fiabilité sur les outils : container, booster  \url{https://groupes.renater.fr/sympa/info/csan}
- connaitre la charge des infras et savoir si le besoin sera réellement accueilli et non juste en théorie
- penser à être moissonnable et pouvoir moissonner d'autres sites (cf métadonnées )
- se servir de PCSC pour réfléchir sur l'orga des meso/infras et pour soumettre des demandes de ressources

   - créer une fédération 
   - créer une authentification unique entre les infras (mobilité des users)- partir de uses cases et de users stories pour adapter et être sûr que l'outil répond au besoin
- attention à ne pas tout focaliser sur que de la technique, se placer au niveau du end-user
- très important de soigner la communication
- la priorité est les néophytes (ceux qui ont le plus besoin) et non les experts
- proposer le référencement des entrepôts
- proposer le référencement de l'archivage
- attention à arriver à un stade opérationnel 
    * Description des services proposés par les infrastructures ? conditions d'accès (techniques et administratives) ?
- Etat des lieux sur les plateformes de calculs/stockage disponibles
    * 2 subdivisions à l'état des lieux document type calc pour faire l'Etat des lieux de NOS cluster ou simple serveur de calculs locaux (hors plateforme) avec des champs(à définir)
- Etat de lieux des forges 
- Ne pas user les gens impliqués.
- choix d'une forge pour dépôt
- quid de la mise jour des données sur le portail (Automatique vs manuelle) 
* pérennité des forges/services?
* peu de temps, peur de ne pas pouvoir aider
- Comment curer les données json ? (manuel ou automatisé avec un plugin installé dans les plateformes)
- Viser trop haut, trop exhaustif et difficile à maintenir ... objectif du V1 ?
 * Pertinence de l'externalisation du dev ? (W)
* Curation des données sur le mi/long terme
* Proximité stockage calcul 
- forge CI-CD pour les images singularity (pull via oras)
- Est-il possible de référencer (en temps réel ?) le niveau de charge des différentes ressources disponibles (stockage disponible, RAM \& CPU utilisée ??), pour guider des utilisateurs vers d'autres plateforme similaire mais disposant de plus de ressources disponibles réellement. Il faut aussi prendre en compte la distance donnée/calcul pour faire des recommandations pertinentes. Un des objectifs étant de limiter le développement en taille de plateformes très populaires quand d'autres sont dispo (développement durable) Objectif pas forcément aligné avec ceux de l'état qui préconise l'utilisation de quelques gros data centers.
- Comment les formulaires sont alimentés?
* Utiliser l'énergie de ce Hackathon pour pousser INRAE à adhérer plus au projet (toutes les infras même petites)
- Sur la gestion des dépendances de la CI Gitlab, envisager de les mettre en cache d'un job à l'autre, cf. \url{https://docs.gitlab.com/ce/ci/caching/index.html}
* difficile de tout bien identifier : identifier les manques
 * Faire une cartographie des compétences des participants
* infrastructures de données de la recherche (pas tout entendu) : veiller au "couplage" avec le futur portail PCS et s'informer sur les initiatives "similaires" (ex: IR Data Terra, TGIR HumaNum, etc.)
* Interopérabilité avec les offres existantes
* Comment on se positionne par rapport à l'IFB ?
    
### Intérêt pour les infras :
    - visibilité
    - soulagement sur l'accompagnement 

   - plus efficace car un humain a une vision plus parcellaire des ressources
   - pourrait éviter au meta-infra de faire le travail de recensement et diffusion (travail en cours à l'IFB)
       * * IFB : fichier excel pénible à remplir 
       * * demande annuelle faite par mail pour mise à jour
       * * inclus des métriques d'utilisation
   - idem pour le cloud mais plus automatique
   - importance pour les infras : pas une vitrine mais un endroit où trouver les ressources
   - bien de mélanger les mondes des infras

## Critères

### warnings
- ne pas chercher l'exhaustivité des paramètres
 - difficulté de faire rentrer l'infra dans les cases de critères (cf besoin de champs libres)
- Peut-être ajouter quelque chose autour de la politique d'accès ? Par exemple, l'Infrastructure Française de Bioinformatique est un institut national (ie pas forcément visible si je cherche quelque chose dans ma région) mais ouvert à toute la communauté des bioinformaticiens/biologistes.
 - comment lister les portails galaxy, rstudio, rshiny 
 - attention sur les détails des paramètres (GPU Xa123, GPU Xv23, ...) et préférer un gros champs libre avec recherche à la google
Critères en cascade (arbre) sur les critères

   * exemple GPU > génération de GPU > version de modèle > RAM des modèles > nombre de GPU par serveurAvec des champs libre à tous les étages de la cascade !!!

   - il faut que les critères soient en phase avec les besoins des utilisateurs (jusqu'où aller dans le détail)- maturité des infra :

   - indiquer si une infra est aussi une infra européenne, infra d'un meta-projet, d'une e-infra de xxxx, ...- les quotas sont dynamiques, doivent-ils être stockés dans des fichiers à part
- attention difficile de spécifier les critères en stockage (bcp de terme + ou - recouvrant et chacun à sa *)
- les mesocentres sont plus compliqués à décrire que les infras GENCI plus homogènes
- rex Daniel : faire tourner un pipeline de bio (venue de comput canada) : impossible de le faire tourné sur x+ infras car toujours des contraintes : ouvertures de ports, télécharger des FSvirtuel, ... Seul le cloud google a réussi à faire tourner le pipeline
    * Description des services proposés par les infrastructures ? conditions d'accès (techniques et administratives) ?
- Pour utiliser le portail, il faut que les personnes soient data aware
- Les conditions d'accès peuvent êtres complexes à gérer
- Les conditions financières d'accès à certaines ressources
* Clients et ressources pas obligatoirement publics
- Au vu des volumes et typologies, comment gérer l'ensemble des données ?

### Critères (utilisateur/besoin/infra/recommandations)
 - inclure des recommandations d'infras en fonction de l'origine de l'utilisateur (ex tel institut/université/labo recommande tel et tel infra = prioriser la présentation ou mettre des tag de recommandation    
  - inclure des warnings de recommandation en fonction de l'origine des utilisateur et de l'infra (pb de sécurité, cloud act)
- Notion de labellisation des infrastructures ?
 - quelques remarques sur les "critères": \url{https://forgemia.inra.fr/ingenum/pcs/-/issues/1}

   * coreNumber: je préciserais si cela s’entend avec "HyperThread" ou non (moi je suis pour compter les cœurs sans hyperthread :p )
   * Storage: Je trouve le champ assez ambigu. Par exemple, est-ce qu'il faut aussi ajouter l'espace utilisé pour la réplication ? Est-ce que je dois noter tous les espaces de stockage de la plateforme même s'il ne sont pas directement lié au cluster (iRODS, sauvegarde) ? Est-ce qu'on doit, en plus de l'espace partagé, comptabiliser les disques locaux des serveurs de calcul ?
   * Storage: Le chiffre donne une bonne idée de la "taille de l'infrastructure" mais n'est pas forcément pertinente pour l'utilisateur (tout les espaces sont-il utilisables ?, Quota ?, etc.)
   * Contact: Je sus mitigé entre mettre un contact technique ou le responsable de la structure. mettre les 2
   * networkBandwith = LAN ou WAN ? Type (Ethernet / Infiniband) ?
   * Ajouter des tags pour décrire en quelques mots clefs l'infrastructure ("mésocentre", "bioinformatique", etc.) ?  

   - prix
   - protocole d'accès
   - logiciels d'accès (winscp, rclone, filezilla, webdrive=outils-payant-photos, )
       * créer des fichiers et structures de données de correspondance entre les softs et les protocoles (et autres fonctionnalités), attention au version
   - taille des fichiers et nombre de fichiers
   - type de fichier (photos, ...)
   - capacité à offrir des fonctionnalités de diffusion (URI, URL, ...)
   - sécurisation des données
       - réplication
       - versionning
       - sauvegarde
       - snapshot
       - ...
   - bande passante
   - performance
   - quota
   - version de MPI 
   - quels logiciels et quelles versions
   - bien formaliser les conditions d'accès aux ressources dans les critères  - connaitre le outils minimaux sur les infras (SLURM, Singularity) 
    - données sous licence
    - softs sous licences
    - aspects organisationnel : condition d'accès

   - appel d'offre ou au fil de l'eau         
   - temps de latence pour avoir accès aux ressources ?
    - unix linux
    - quel type de scheduler ?
    - containeurisation possible ?
    - permettre l'adéquation avec le niveau de compétences de l'utilisateur (en lien avec les outils et services)
    - prix (et couts cachés)
    - pouvoir héberger un site web qui accède aux ressources
    - protocoles d'accès 
    - types montages possible
    - critères de mobilité:

   *         possibilité de classer les centres de ressources par ceux qui se ressemble
   - possibilité de comparer les ressources sélectionnées
   - accompagnement, doc, formation proposée comme un service
   - VO de la grille acceptée par l'infra
   - comment se place PCSC par rapport aux feuilles de route des e-infra (elixir, IFB, ...) ?
   - HDS
   - PGD (acceptation du users si PGD)
   - certifications (RDA, HDS, )
   - PGDready
   - coût écologique (C02, e-, ...) des infras (efficience des salles/clim, types de stockage, ...) +
   - quotas: 
       - par user
       - par groupe    - prendre en compte la bande passante en fonction du jeu de données et les distances user/infra
    - type de données (texte (génome) binaires (séquençage ...)
    - thématique
    - niveau de droits, liberté d'installation
    - ouverture de ports possible
    - quels ports ouverts
    - compilateurs
     - critères d'acceptation de l'accès aux infras

   - géographie (région, ville, zones, ou GPS tout simplement)
       - financement français
       - recherche publique avec publication des à la clef 
       - le projet a-t-il un DMP (publique / pas publique) 
   - aspect environnementaux (efficacité énergétique, empreinte carbone) et sociétaux (politique personnes à handicap, stages, CDD/CDDI, ...), \url{https://ecoinfo.cnrs.fr/}
   - DMP 
   - prix
       - prix à payer par le end-user
       - vrai prix (coût environné (bât, fluide, )par heure de calcul, par To, etc)
       - coût du réseaux (Renater) du transfert entre le user et l'infra (aller/retour), 
       - coût des E/S
       - prix stockage
           - rapide
           - lent
           - capacitif
           - object/filer      - service de multitenant sur 

   - équivalent CO2 du besoin (en moyenne ou par infra), en électricité en Watts, + équivalent en km avec une voiture
   - hébergement web (galaxy, et autre visualiseurs, ex : genombrowser)
   - banques de données
   - service de base données à la demande
   - infras spark, hadoop : HPDA
   - Outils pré-installés sur les plateformes (+version) : librairies installées
   * utilisation possible de conteneurs (types singularity) et/ou image machine virtuelle
   * possibilité d'installer des librairies au niveau utilisateur
   - RGPD compliance des plateformes
   - Certification gestion des données médicales
   - Certification IBISA, trust \& co. 
   - nombre de coeurs(0-5000), ordonnanceur(sge,slurm,..), outils de gestion des logiciels (module,lmod,spack...), outils indispensables(clustershell,puppet,...), monitoring(ganglia,grafana) .....etc
   - Critères de recherche en fonction des besoins => ie. guider avec des questions générales puis de plus en plus ciblées pour identifier les ressources à utiliser
   - Quels sont les outils disponibles/utilisables sur une plateforme ? (SLURM, Singularity, ...)- Focus sur les conditions/contraintes d'accès aux ressources (administratifs)
* Conservation des données output dans les critères
* Modalités d'accès

### Extrait d'infos recueillis par l'Institut Français de Bioinformatique (IFB) auprès des plateformes fédérées

                   * Plateforme (nom)
                   * Email de contact (direction)
                   * Région
                   * Ville
                   * Main affiliation (CNRS/INRAE/etc)
                   * Institute / University
                   * Current site (lieu d'hébergement de l'infra)
                   * Cluster / Cloud
                   * Contact email for budget

                   * Nom des ressources ou services (exemple: cluster, cloud, service web)
                   * Types de services assurés sur cette infra (Analyse de données pour des usagers, Hébergement de comptes usagers (calcul et stockage), Formation, Développement de ressources logicielles (outils, DB) originales, Déploiement de ressources web (outils, DB), Autres types de service (préciser), Remarques)

                   * Équipements:
                   - Cœurs CPU (sans hyperthread)
                   - Lames GPU
                   - Stockage rapide (To)
                   - Stockage capacitif (To)
                   - Stockage total
                   - Sauvegarde sur bande ou autre type de matériel (To)
                   - Besoins en heures CPU (quand l'infra n'est pas gérée par la plateforme)

                   * Finance
                   - Dépenses équipement de l'année
                   - Coût directs hébergement (fluides, RH ASR, sécurité, ...)
                   - Fonctionnement hors hébergement de l'infra

                   * Personnel        
                   - ETP permanents associés aux services
                   - ETP CDD associés au service
                   - ETP permanents idéaux pour assurer les services
                   - Répartition ETP: Analyse de données pour des usagers, Hébergement de comptes usagers, Formation, Développement de ressources logicielles (outils, DB) originales, Déploiement de ressources web (outils, DB), Autres types de service (préciser)

                   * Coût
                   * Coût unitaire (Coût environné par unité en absorbant les autres coûts: personnel, amortissement, maintenance, hébergement)
                   * Coût du calcul CPU (pour 1000H)
                   * Coût du calcul GPU
                   * Coût stockage capacitif
                   * Coût stockage performant
                   * Coût stockage mixte

                   * Usage
                   * Nombre d'utilisateurs actifs
                   * Heures de calculs
                   * ...

## Ontologies, vocabulaires
- lien vers les ontologies : \url{https://lov.linkeddata.es/dataset/lov/terms?q=cpu}
- ontologies pour les personnes : \url{http://xmlns.com/foaf/spec/} et pour les relations : \url{https://vocab.org/relationship/}
- permettre au fournisseur de ressources de remplir ses caractéristiques via un outil graphique :
     - normé
     - facile à remplir
     - visuel
     - facile de se rendre compte si la description correspond bien à l'infra/services
     - ex :  graph rdf (\url{https://dist.neo4j.com/wp-content/uploads/20170617195358/RDF-graph-example-graphconnect.png?)} -> schema json rdf
     /!\ attention ça peut devenir une usine gaz 
- usage SKOS ou OWL ? utile d'aller jusque là ?
- outils : Protégé : \url{https://protege.stanford.edu/} (version web possible : \url{https://protegewiki.stanford.edu/wiki/WebProtegeUsersGuide)}
- Existe-t-il des vocabulaires mentionnant les concepts manipulés (CPU, RAM, stockage...) : regarder dans LOV (\url{https://lov.linkeddata.es/dataset/lov)}, ie. \url{http://cookingbigdata.com/linkeddata/ccinstances/}
* gros travail sur la terminologie, vocabulaires !
 - Les conditions d'accès peuvent êtres complexes à gérer
Au vu des volumes et typologies, comment gérer l'ensemble des données ?

## formats des données
- une infra a plusieurs "infra" en interne (effet poupée russe des infras)
- pointer des personnes contacts dans l'infra mais aussi généralistes (centre, institut, unité, ...) en fonction du domaine (cluster, data, cloud)
- format stable
- description des infras comptes twitter, Facebook
- les quotas sont dynamiques, doivent-ils être stockés dans des fichiers à part
* Définition des données en JSON (W)

## Documentation
- attention à la doc qui va avec les critères
 
- utilisation des platforms (la base de l'utilisation) : cours sexy

   - workshop
   - webinaire
   - video explication
   - tutos pas à pas- rex : passage du local au distant difficile pour le end-user (EB), accompagnement important

   - passer du temps avec les utilisateurs pour bien connaitre leur pratiques pour pouvoir les accompagner
           -> long mais indispensable, ne avoir une approche trop technique, "descendre" à son niveau
           -> il faut leur faire faire le premier pas, ça ne viendra pas des end-user tous seuls- parler des outils/méthodes pour travailler sur des ressources distantes
- parler aux users avec leurs mots
- danger à trop structurer la doc, passer par des mot clés, des cas usages
- bien comprendre les différents publiques de PCSC (différents niveaux)
- la priorité est les néophytes (ceux qui ont le plus besoin) et non les experts
- les experts seront plus intéressés par l'OAD
 Comment impliquer les experts dans l'accompagnement aux néophytes

   - l'expert explique mais on le soulage du travail de rédaction afin qu'il "donne" plus facilement
   - l'expert est plus enclin à donner des infos à ses pairs qu'à un inconnu ou une personne qui n'est pas de son domaine- REX end-user sur sa découverte de l'info = extrêmement chronophage, décourageant, , 

   * difficultés pour comprendre
       - cluster de calcul
       - ssh
       - containers
       - env virtuels
       - linux
       - message d'erreur abscons 
       - quels outils (pas MS Word pour un script, n'est pas une évidence pour le non-informaticien)
   * n'est pas un problème quand on est "jeune" :
       - matériel : coeur, ram, ...- connaitre ce que veut faire le end-user est finalement assez difficile, lui-même ne sait pas toujours
- être capable de reformuler la demande du néophyte est une difficulté en soit
- néophytes pas toujours réceptifs (comment avancer ?)
- le portail doit dédramatiser les aspects calcul/stockage/cloud, il faut faire tomber la peur de ces outils
- aller vers l'autonomie des utilisateurs
- important d'avoir rex pour s'améliorer mais aussi de diffuser les rex pour promouvoir l'outils. On fait confiance à ses pairs

   - webcast, interview écrite, webinaires flash, tutos, TP d'utilisation, classe virtuelle- intégrer des néophytes comme beta-testeurs
- construire un portfolio via des cas d'usage (entrée) vers des rex
- recenser les questions des utilisateurs : connaitre leurs problèmes, connaitre leur vocabulaire
- identifier des key-users et les interroger 
- définir les termes : exemple cloud, container, machine virtuelle
- construire mooc ? mooc existants ?
- créer une liste de formations curées (à présenter sous forme de parcours de compétences ?)  
- doc step by step, progressivité, ne pas montrer la montagne à apprendre, la longueur et la pente du chemin à parcourir
- expliquer l'intérêt d'un cluster de calcul ou d'infra distantes / local
- expliquer comment gérer ses données, son stockage, gérer des gros jeux de données, outils adaptés
- trois étapes pour réussir :

      1.  écouter le besoin du end-user
      2.  traduire le besoin du user grâce l'expertise de l'accompagnant
      3.  traduire le besoin en technique pure du besoin- lancer une enquête de uses-cases ? 

   - sinon demander aux :
       -  correspondants
       -  informaticiens d'unités de recherche
   - danger :
       - peur du flicage
       - peur de l'avis de l'autre- rex Daniel : faire tourner un pipeline de bio (venue de comput canada) : impossible de le faire tourné sur x+ infras car toujours des contraintes : ouvertures de ports, télécharger des FSvirtuel, ... Seul le cloud google a réussi à faire tourner le pipeline
 - tuto sur outils (+1)
   - Communication par rapport aux chercheurs (local vs. collectif) => Vulgarisation, pédagogie
   - workshop sur les outils de containerisation, reproductibilité dans le calcul scientifique
- Catalogue de formation d'usage des différents types de ressource, permettant de partager les bonnes pratiques
- Documentation/Définitions; Pour les utilisateurs de l'outil (ils ne maîtrisent pas forcément toute la terminologie ou peuvent avoir des doutes). Pour les fournisseurs, ces définitions pourraient être incluses dans le schéma de la plateforme pour s'assurer de la cohérence.
* Lien données/calcul
   
### ChatBot
 chatbot (même temporaires pour co-construire : 

   - permet d'avoir des questions réelles des end-users
   - il faudrait des humains derrière (au moins durant un temps de test/construction)         - construire une base de connaissance
mettre en place un chatbot :

   - interface plus simple que d'aller fouiller dans un doc immense
   - à la fin demander si le user est content et si non alors lui demander son avis (chatbot ou site) : note via des étoiles + texte libres possible





## OAD
    - comment construire le moteur ? 
            - s'inspirer des success stories : 

       - zenodo (moissonnage d'infos bien fait, cf invenio) 
           *         des outils comme dataverse ou invenio ne seraient-ils pas pertinents ?  - des outils comme dataverse ou invenio ne seraient-ils pas pertinents ?
  - inclure des recommandations d'infras en fonction de l'origine de l'utilisateur (ex tel institut/université/labo recommande tel et tel infra = prioriser la présentation ou mettre des tag de recommandation

   - une version simple pour les débutants + recherche avancée
   - case à cocher disant si on est débutant => besoin d'accompagnement humain ou solution "facile" 

   - attention à être capable de proposer une interface simple pour les débutantsCritères en cascade (arbre) sur les critères

   * exemple GPU > génération de GPU > version de modèle > RAM des modèles > nombre de GPU par serveurAvec des champs libre à tous les étages de la cascade !!!
- connaitre la charge des infras et savoir si le besoin sera réellement accueilli et non juste en théorie
- interface difficile d'accès pour le end-user néophyte
- proposer un outils qui montre le temps de calcul sur un portable, sur une workstation, des clusters tier3/2/1/0/ , la grille

   - que ce soit automatique par défaut dans l'OAD ?- Mettre un mécanisme de check du last update des descriptions des infra pour pouvoir relancer les responsables pour vérifier la cohérence
- Différents parcours dans l'OAD : parcours expert, par profil d'utilisateur/usage, catalogue
ex: traitement de données avec beaucoup de données en entrée et/ou sorties (données climatiques, télédétection, lancement de modèles)
dans le cas de calculs hors du cadre de recherche (pour utilisateurs, diffusion de résultats de recherche pour des acteurs économiques), quelle possibilité ?
- Formulaire recherche simple + avancée
- 3 gros boutons : choix primaries : matériel / logiciel / utilisation
clic : déplie les items correspondants 
matériel : portail actuel
logiciel : librairies + logiciels + versions + conteneurs 
- ? : protocoles supportés : S3, NFS, ssh, pour clusters 
- utilisation : génomique, mécanique des fluides, traitement de données, etc.
- prise en compte du profil utilisateur pour savoir si les solutions sont payantes ou non
- choix gros grain : prix/ payant ou non
- OAD : les menus de filtres (ex: localisation) ne devraient contenir que des valeurs pour lesquelles il existe des résultats. Solution (?) : alimenter les listes avec les valeurs présentes dans le json. 
- Les conditions d'accès peuvent êtres complexes à gérer


## UX
- Peut-être ajouter quelque chose autour de la politique d'accès ? Par exemple, l'Infrastructure Française de Bioinformatique est un institut national (ie pas forcément visible si je cherche quelque chose dans ma région) mais ouvert à toute la communauté des bioinformaticiens/biologistes.
pointer des personnes contacts dans l'infra mais aussi généralistes (centre, institut, unité, ...) en fonction du domaine (cluster, data, cloud)
- salon de discussion : pour de l'aide ponctuelle/interactive 
   => exemple tchap : salon de discussion avec grosse plus value  interministériel 

       - pas de maintenance
       - facile à mettre en place
       - outil souverain et sécurisé
       - inscription institutionnelle via mail pro mais 
           - pas d'inscription autonome pour hors des ministères (mais invitation possible)
           - besoin d'une clef
           - si perte de clef alors perte des discussions
       - gestion des clefs de chiffrement peut-être compliqué
       - pas de sous-salon possible (pour l'instant, à l'avenir ?) - inclure des recommandations d'infras en fonction de l'origine de l'utilisateur (ex tel institut/université/labo recommande tel et tel infra = prioriser la présentation ou mettre des tag de recommandation
  - attention à la doc qui va avec les critères
- attention à être capable de proposer une interface simple pour les débutants
- les fournisseurs remplissent leurs données (décentraliser le remplissage)
- avoir un contact pour PCSC facile à trouver : tel et/ou mail et/ou salon de discussion

   - pb technique avec PCSC
   - pb sur les valeurs des données
   - pb d'utilisation de PCSCAOD : mettre une description standardisée et sympa dans les choix + un page complète et sympa pour chaque infra

   - montrer aux utilisateurs le taux de charge en calcul des infras n'a pas de sens pour le calcul, ni pour le stockage- il ne faut pas que le portail perde les users
- penser à être moissonnable et pouvoir moissonner d'autres sites (cf métadonnées )
- maturité des infra :

   - indiquer si une infra est aussi une infra européenne, infra d'un meta-projet, d'une e-infra de xxxx, ...- lier le glossaire à l'oad

   - donner la définition des termes en dynamique depuis l'OAD- un endroit unique pour mettre les descriptions des ressources
- description des infras comptes twitter, Facebook
- attention à ne pas tout focaliser sur que de la technique, se placer au niveau du end-user
- interface difficile d'accès pour le end-user néophyte
- soigner l'UX de la doc et aussi doc/OAD
- la priorité est les néophytes (ceux qui ont le plus besoin) et non les experts
- les experts seront plus intéressés par l'OAD
- intégrer des néophytes comme beta-testeurs
- proposer un forum d'entre-aide
- proposer un canal mattermost : dynamique, interactif
- si questions publique : honte de s'exprimer devant les autres 
- proposer un outils qui montre le temps de calcul sur un portable, sur une workstation, des clusters tier3/2/1/0/ , la grille

   - que ce soit automatique par défaut dans l'OAD ?- permettre au fournisseur de ressources de remplir ses caractéristiques via un outil graphique :
     - normé
     - facile à remplir
     - visuel
     - facile de se rendre compte si la description correspond bien à l'infra/services
     - ex :  graph rdf (\url{https://dist.neo4j.com/wp-content/uploads/20170617195358/RDF-graph-example-graphconnect.png?)} -> schema json rdf
     /!\ attention ça peut devenir une usine gaz 
- à la fin demander si le user est content et si non alors lui demander son avis (chatbot ou site) : note via des étoiles + texte libres possible
- Options partie logicielle dans le portail
- possibilité de comparer les résultats de recherche + afficher seulement les différences
- bison futé du taux d'occupation/disponibilité des clusters
chaque plateforme doit remonter des informations sur l'occupation de leurs queues et sur la disponibilité à terme
- Review et retex sur les plateformes
- Ajouter un lieu d'échange (tips \& tricks, retours, publis...)
- Ouverture d'un salon public sur Tchap (tchap.gouv.fr)
- Liens vers les chartes d'utilisation et modèles économiques ?
- Pas seulement des liens vers des infras; il faudrait créer du lien (échange de tips, publis, projets etc.)
- Feedback sur les réponses de l'OAD (pertinence des réponses ...)
- Feedback global des utilisateurs (retour d'expertise)
- Ajout d'un champ tag optionnel dans json ? chat bot ? possibilité de poser des questions sans quitter la page
En alternative d'un chat bot, faire un système de queries fléchées questions/réponses avec menus déroulants (ou autre) pour guider sur les outils à utiliser pour accomplir une tâche ; e.g., un utilisateur veut faire une étude transcriptomique sur un eucaryote (taille du génome) avec 3 répétitions biologiques en utilisant des reads Illumina, que doit-il chercher?
explication de terme
 - 2 modes de contributions dont 1 adapté aux non familiers aux outils
* Soigner l'UX
* Au vu des volumes et typologies, comment gérer l'ensemble des données ?
 * Dynamisme, interaction dans le portail


## API 
 - API pour mettre à jour 
   - surtout très utile pour ce qui est changeant (logiciels, lib, ...) - API s'oppose/complémentaire à la génération du json complet par le fournisseur d'infra
- penser à être moissonnable et pouvoir moissonner d'autres sites (cf métadonnées )
- mettre un API en requête
- mettre en place une API afin que d'autres portails d'aide aux chercheurs puissent regrouper les infos
* Est-ce qu'on envisage que les données soient interrogeables par autre chose que le portail (API, SPARQL...) ?

## Gouvernance
qui a le droit de modifier les données des infras

- péremption des données (disparition des données si pas mises à jour après x temps) : il faut mieux ne pas avoir de données que d'avoir des données obsolètes
- les fournisseurs remplissent leurs données (décentraliser le remplissage)
- relancer les fournisseurs avant échéances    
- qui sera responsable d'information ?

- auteur de la fiche- mettre un responsable de fiche, qui rempli la fiche et qu'on sache qui il est (identification !!)

-> qualité, pas d'usurpation d'identité
-> celui qui remplit fait parti de l'infra
* 2 choix possibles pour remplir/mettre à jour les fiches : 
   - petit groupes qui remplit
   - chacun maintient sa fiche (valeur ajouté)- très important de soigner la communication
- Mettre un mécanisme de check du last update des descriptions des infra pour pouvoir relancer les responsables pour vérifier la cohérence

### Critères :
- temps de vie des données (engagement des infras sur la rétention)
- quid des entreprises qui proposent des infra
- Facturer l'usage du portail au privé pour financer l'expertise ?
- Les tutelles fournissent des ressources en ETP au projet
- Ouverture d'un salon public sur Tchap (tchap.gouv.fr)
- Quelle infra, quels RH ? Répartir entre les institutions pour limiter les coûts pour chacun.
- S'inspirer du projet plume, les contacter ?
- Monétisation des services pour responsabiliser les utilisateurs ?
- Faire un roadmap du projet pour savoir où on va (MVP, V.1, V.2 ? ... ne pas tout faire d'un coup, prioriser, cycle de dev du portail ? vie du portail => comment faire demande d'ajout de fonctionnalités au cours de la vie du portail ?)
- Quelle forge pour la version finale ?
* Pertinence de l'externalisation du dev ? (W)
- Quelle gouvernance ? Qui sont les acteurs et quels rôles auront-ils ?  plutôt orienté utilisateurs (on s'assure de répondre aux besoins)  / plutôt orienté fournisseurs (on s'assure de la mise à jour des données MAIS risque que le portail devienne une vitrine commerciale) . Il faut trouver un équilibre
- Pour faciliter la mise à jour des données par les infrastructures, le portail doit se positionner comme fournisseur d'informations pour des sites plus spécifiques qui référencent ces mêmes services/ressources. Les données d'une e-fra ou d'une organisation par exemple doivent pouvoir être intégrées facilement dans un site web au moyen d'un widget, iframe, contenu json, etc.  
- Quid des offres commerciales avec lesquelles des instituts de l'ESR auraient des accords ? Pourraient-ils être visibles parmi les ressources de l'outil ?
- Quelles interactions avec les autres inventaires (IFB, France Grille...) ? Le portail permet d'industrialiser le recensement des ressources informatiques disponibles.
* Curation des données sur le mi/long terme
- dans un premier temps, le portail est en français. On peut viser un portail bilingue, notamment pour des questions de visibilité (dont au niveau européen), mais aussi pour nos utilisateurs non francophones. Les définitions pourront présenter les termes en français et en anglais.
* comment une carto (à jour, utile, perenne) se mettra en place : ce n'est pas que de la technique
* Clients et ressources pas obligatoirement publics
* Comment on se positionne par rapport à l'IFB ?
* infrastructures de données de la recherche (pas tout entendu) : veiller au "couplage" avec le futur portail PCS et s'informer sur les initiatives "similaires" (ex: IR Data Terra, TGIR HumaNum, etc.)
* Mise à jour des informations ?
* Comment garantir que les données soient à jour et pertinentes (+qualité, +validation) ?
