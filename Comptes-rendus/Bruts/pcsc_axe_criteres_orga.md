
# Grille auto-organisation Hackathon PCSC

# Axe Critères



### Nom du groupe : 

## 

Pad de travail : [https://etherpad.in2p3.fr/p/pcsc\_axe\_criteres](https://etherpad.in2p3.fr/p/pcsc\_axe\_criteres)



## Membres inscrits (chacun rajoute son nom)

*Lister les membres et se présenter*

   * Pierre Gay (fournisseur de ressources, MCIA: mésocentre aquitain)
       * critères définitions infrastructures
   * Fred de Lamotte (INRAe - Montpellier)
   * David Benaben (admin sys, INRAE, Bordeaux, Centre de Bioinformatique de Bordeaux)
       * lien avec IFBioinformatique
   * Cyril Jousse (Univ Clermont Auvergne) .
   * Pierre Adenot (INRAE, Jouy-en-Josas)
   * Christophe Moisy (INRAE, Bordeaux) : ing en traitement de données
       * 

   * Nadia Ponts (INRAE, Bordeaux)
   * Loïc HOUDE (admin sys,INRAe PACA,MathNum/Biostatistique) Je suis admin d'un petit cluster de calcul  pour les unités du  centre PACA interessé  par tout (mais pas l'ame d'un entrepreneur ni d'un meneur)
   * Zenaida TUCSNAK (CNRS, INSHS, Bordeaux)
       * intéressée critères attendus par utilisateurs
   * Estelle Ancelet (INRAE, Toulouse, Unité MIAT). 
       * Développeuse, administratrice instance Galaxy hors bioinfo, utilisation de base du mesocentre meso@LR 
       * expérience de recherche d'instance : apport de quelques critéres
       * lien groupe doc
   * Olivier Porte (CNRS)
       * observateur groupe OAD
   * Pierre Adenot (INRAE jouy, plateforme imagerie)
       * intéressé par portail, apport expérience utilisateur groupe format
   * Emilie Lerigoleur (CNRS, UMR GEODE Toulouse)
       * intervention à la marge car dans 2 autres groupes (UX et doc), peux jouer le rôle de "boulet relecteur des critères proposés", donc dans un second temps !
   * Eric Cahuzac (INRAE, DipSO, Num4Sci)
       * A la marge également, pour faire le lien avec UX, afin que l'interface exploite pleinement les critères? Donc suis de loin.
   * Patrick Chabrier (INRAE, MathNUM, Toulouse, MIAT)
       * Informatique, Modélisation et Simulation
   * alexandre Dehne Garcia (ancien adminsys calcul/stockage, j'ai accompagné au jour le jour une unité de recherche sur leurs problèmes en calcul et stockage, en interne comme en externe)
   * Vincent Nègre (INRAE Montpellier), adminsys stockage/calcul.




## Rôles

Un rôle correspond à une “casquette”, chacun peut changer de casquette ou en porter plusieurs.

Vous pouvez changer les noms des rôles - A vos bonnes idées....

   * ***Huggy les bons tuyaux**** : chargé(s) de faire le lien avec l’équipe d’organisation et les autres groupes **[]--> on a besoin de qq1[]***
   * *Rôles spécifiques aux réunions  (rôles tournant)*
       * ***scribes**** : chargé de prendre les notes pendant les réunions*
       * ***cadenceurs**** : chargé de gérer le temps pendant les réunions*
   * ***La Balance**** : présente les résultats du groupe en plénière —> penser à le nommer quand un retour du groupe en plénière est annoncé **[]- à définir en visio cet aprem[]***
       * Pierre Gay - pour la réunion du 05/06/2020
   * ***Le Boulet**** : rappelle que ça doit être adapté à tous les types d’utilisateurs - Pierre Adenot, Estelle *
   * ***Dirty Harry**** : s’assure que tout avance, que les membres ne sont pas perdus **[]--> on a besoin de qq1[]***
## 



## Cadrage (léger)

   * Objectifs du groupe
       * définir les critères pour décrire :
           * les fournisseurs de ressources
           * les ressources
           * les utilisateurs
           * les besoins des utilisateurs
           * recommandation des entités : 
   * Liens avec autre groupe de travail ? Coordinations éventuelles
       * ontologie
       * OAD ?
       * format de données
           * les critères sont évolutifs,
           * certains vont apparaitre et disparaitre avec le temps,
           * il y a des critères en cascade, ex  : 
               * y at-il du gpu
               * génération des GPU
               * modèle du GPU
               * ram GPU
               * combien de GPU par serveur
   * Moyens nécessaires (temps, outils)
   * Risques
   * Évaluation des risques
   * Définir un "minimum" de critères ou des "blocs" de critères (emboités à tiroirs)
   * Échange "allé/retour" avec Ontologie/Format/Utilisateur (UX)/... à faire régulièrement


### Quels sont les Risques ===> les Solutions possibles correspondantes :

*risque x ===> solution de x *  

   * être trop précis, difficile à remplir par les fournisseurs de ressources ===> faire valider les critères par un groupe représentatif de fournisseurs 
   * être trop précis, critères pertinents pour personne ===> se baser sur des uses cases réels ou prévoir des critères emboités (les génériques-obligatoires et les spécifiques ou plus précis qui sont recommandés mais optionnels)
   * avoir des critères inadaptés au fournisseur ===> proposer des critères fournisseur-dépendant ou type-de-fournisseur-dépendant
    

 

 

 

  

 

   * Plan d’action / tâches / affecté à qui ? —> évolutif dans le temps, on se détend...




Document de travail: [https://etherpad.in2p3.fr/p/pcsc\_axe\_criteres](https://etherpad.in2p3.fr/p/pcsc\_axe\_criteres)



Prochaines discussions visio, [https://meet.jit.si/pcsc\_criteres](https://meet.jit.si/pcsc\_criteres) :

   * lundi 15/06 : 14h00
   * mardi 16/06 : 11h00




**VISIO du 08/06 :**

 

Présents: Alex, Pierre, David, Christophe, Pierre-Emmanuel, Cyril

    

Critère fait une liste la plus exhaustive, UX fera le tri "neophyte vs expert" ?

TODO :

   *     prévoir une réunion avec ontologie (Cyril balance 08/06 à 13h30)




**VISIO du 09/06 :**

 

Présents: Tov, Vincent, David, Christophe



Discussion mise en "schéma": [https://forgemia.inra.fr/hackathon-portail-calcul/hackathon\_organisation/-/tree/master/Crit%C3%A8res](https://forgemia.inra.fr/hackathon-portail-calcul/hackathon\_organisation/-/tree/master/Crit%C3%A8res)

Coup d'oeil sur portail EOSC (pas beaucoup d'infos à en tirer à priori)

Affinage critères (fournisseur ressource)



**VISIO du 10/06 :**

 

Présents : Oana, Pierre, Tovo, Vincent, Christophe, Cyril, Pierre-Emanuel, David



Affinage critères (datacenter) 

Discussion sur les relations (fournisseurs  <-> ressources, ressource de calcul <-> ressource de stockage,  ressources <-> datacenter, etc)

Idée de "meta-ressources" (regroupement de ressources) de "partition", etc.












