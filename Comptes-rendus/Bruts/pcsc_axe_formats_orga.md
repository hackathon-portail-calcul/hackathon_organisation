
# Grille auto-organisation Hackathon PCSC

# Axe Formats des fichiers de données 

## 

## Membres inscrits

*Lister les membres et se présenter*

   * Patrick Chabrier (INRAE, MathNUM, Toulouse, MIAT)
       * contexte : Informatique, Modélisation et Simulation
       * motivation : Participation à la production du schéma des données
   * Zenaida Tucsnak: CNRS Bordeaux (INSHS). J'aimerais apprendre et faire des tests
   * Anne Laurent (Université de Montpellier, LIRMM). je peux suivre les travaux du groupe et tenter d'y faire des propositions
   * Arnaud - lien avec groupe ontologie
   * alexandre Dehne Garcia (INRAE, orga)




WARNING: le PAD est retourné, à partir de Maintenant :) Ce qui est frais est à partir de là:

    

    

    

    

---> Prochaine réunion : Jeudi 11 juin à 16h

   * Terminer la définition du rôle de l'axe
       * Spécification ?
       * Documentation technique (répondre à comment faire) ?
       * Rendre plus robuste l'existant, + valide?
   * Statuer sur JSON-LD : [https://json-ld.org/](https://json-ld.org/) 
       * retour du groupe ontologie, attention :
           * ne pas confondre json-ld et schema json <--J'avais vu ça?
           * savez l'interpréter et l'exploiter ? :
               * par le portail 
               * par des tiers
   * En passant un peu + de lecture :) [https://www.ionos.fr/digitalguide/sites-internet/creation-de-sites-internet/tutoriel-json-ld-dapres-schemaorg/](https://www.ionos.fr/digitalguide/sites-internet/creation-de-sites-internet/tutoriel-json-ld-dapres-schemaorg/)
   * Une autre façon de valider le schéma ? 


## Notes concernant la réunion du 8 juin à 8h

   * Rappels sur JSON
   * Visite du Schéma actuel du POC
   * La référence du coup en matière de schéma est bien [https://json-schema.org/](https://json-schema.org/)
   * Discussion sur Go no Go de l'approche json-ld...quel risque?
   * Divers échanges sur le lien avec HUGO
   * Question sur la notion de référence?






## Rôles

Un rôle correspond à une “casquette”, chacun peut changer de casquette ou en porter plusieurs.

Vous pouvez changer les noms des rôles - A vos bonnes idées....

   * ***Huggy les bons tuyaux**** : chargé(s) de faire le lien avec l’équipe d’organisation et les autres groupes*
   * *Rôles spécifiques aux réunions  (rôles tournant)*
       * ***scribes**** : chargé de prendre les notes pendant les réunions*
       * ***cadenceurs**** : chargé de gérer le temps pendant les réunions*
   * ***La Balance**** : présente les résultats du groupe en plénière —> penser à le nommer quand un retour du groupe en plénière est annoncé*
       * 5/06 Patrick
   * ***Le Boulet**** : rappelle que ça doit être adapté à tous les types d’utilisateurs*
   * ***Dirty Harry**** : s’assure que tout avance, que les membres ne sont pas perdus*
## 



## Cadrage (léger)

   * Objectifs du groupe
définir le schéma du système stockant les données, en se servant de ce qui avait déjà été proposé dans l'outil existant

Comment on stocke les critères : faire du json ? --> le groupe validerait à priori ce choix

Sélectionner un format de données et aller plus loin pour développer le schéma

Schéma évolutif dans le temps pour pouvoir intégrer des critères et en faire sortir

Comment interroger et naviguer, flexibilté schéma de données

Comment faire un schéma interopérable sémantiquement

Production d'un méta-modèle (définition à accorder)



Formation des participants --> Tovo va proposer un temps de formation la semaine prochaine



Schéma actuel : [https://forgemia.inra.fr/hackathon-portail-calcul/existant/-/blob/master/data/platform.schema.json](https://forgemia.inra.fr/hackathon-portail-calcul/existant/-/blob/master/data/platform.schema.json)

-------------------

**--> autre groupe - voir après**

Faire des recommandations sur les données à stocker, formats à privilégier ou éviter

Bibliothèque de formats de fichiers à utiliser pour pouvoir enregistrer des données dans ces portails

Tests, bonnes pratiques** **

----------------------

   * Liens avec autre groupe de travail ? Coordinations éventuelles
       * consommateur des infos du groupe critères
       * lien groupe ontologies - métadonnées pour interopérabilité / voc controlés
   * Moyens nécessaires (temps, outils)
       * Définir les compétences pour pouvoir faire appel à la communauté




### Quels sont les Risques ===> les Solutions possibles correspondantes :

*risque x ===> solution de x *   

    

 

 

   * Plan d’action / tâches / affecté à qui ? —> évolutif dans le temps, on se détend...
       * Repréciser les objectifs par écrit dans le pad
       * Formation avec Tovo : 8h-9h lundi 8/06 [https://meet.jit.si/pcsc\_formats](https://meet.jit.si/pcsc\_formats)
       * Se fixer des dates pour se répartir les actions


## Synthèse pour la plénières de vendredi à 11h

   * Sur la base d'une seule visio réunion jeudi 4 juin à midi
   * Réunion très productive
       * Définition des objectifs
       * Cahier des charges
       * Périmètre
       * Type d'actions
   * Les questions :
       * Établir des bonnes pratiques en terme de stockage d'information (format d'image, format texte,...)
       * Veille techno, ou état de l'art pour un si de ce type
       * Production, opérationalité
       * Marge de manœuvre vis à vis de la solution existante, remise en question
   * Quelques points d'appui
       * S'appuyer sur la solution existante (JSON pour les données, JSON pour le schéma)
       * Définir le schéma des données
           * Modèles, versus méta-modèles, en a-t-on besoin
   * Contraintes...très intéressantes
       * Anticiper la flexibilité de OAD
       * Anticiper l’interopérabilité sémantique (URI dans le scléma? json 2 rdf??)
       * Schéma évolutif ( typologie de critères, plutôt que critères)
       * Pas forcément établir une strurcture définitive, plutôt structure opérationelle opérationelle durable
   * Interactions avec les autres axes
       * Axe Critère
       * Axe Ontologie
   * Quelques Faiblesses(encore dans l'auto orga)
       * Compétences?
       * Rôles?
       * Feuille de route?(réunions?)
       * Manquer de force?
   * Point fort :)
       * En mesure de préciser les objectifs
       * Formation avec Tovo : 8h-9h lundi 8/06 [https://meet.jit.si/pcsc\_formats](https://meet.jit.si/pcsc\_formats)


## Pédagogie Inversée en vue de la formation de lundi matin à 8h

   * JSON([https://www.ecma-international.org/publications/files/ECMA-ST/ECMA-404.pdf)](https://www.ecma-international.org/publications/files/ECMA-ST/ECMA-404.pdf)) ça a le mérite d'être clair.
       * ECMA : [https://en.wikipedia.org/wiki/Ecma\_International](https://en.wikipedia.org/wiki/Ecma\_International)
   * HUGO Data Templates : [https://gohugo.io/templates/data-templates/](https://gohugo.io/templates/data-templates/) , on dirait que tout est là :)
   * What is a schema : [https://json-schema.org/understanding-json-schema/about.html#about](https://json-schema.org/understanding-json-schema/about.html#about)
   * Example : [https://json-schema.org/learn/miscellaneous-examples.html](https://json-schema.org/learn/miscellaneous-examples.html)






## Questions des apprenants

   * Limitations venant de HUGO?
   * Piste pour tests et validation
   * comment mettre des ontologies dans le json 


## Et aussi

   * Le graal ?
       * [http://www.markus-lanthaler.com/research/on-using-json-ld-to-create-evolvable-restful-services.pdf](http://www.markus-lanthaler.com/research/on-using-json-ld-to-create-evolvable-restful-services.pdf)
       * [https://fr.wikipedia.org/wiki/JSON-LD](https://fr.wikipedia.org/wiki/JSON-LD)
       * [https://json-ld.org/](https://json-ld.org/)
       * [https://www.w3.org/2018/jsonld-cg-reports/json-ld/](https://www.w3.org/2018/jsonld-cg-reports/json-ld/)
       * [https://json-ld.org/playground/](https://json-ld.org/playground/)
       * [https://data.inrae.fr/api/datasets/export?exporter=schema.org\&persistentId=doi%3A10.15454/BVXD7I](https://data.inrae.fr/api/datasets/export?exporter=schema.org\&persistentId=doi%3A10.15454/BVXD7I)
       * [https://www.ionos.fr/digitalguide/sites-internet/creation-de-sites-internet/tutoriel-json-ld-dapres-schemaorg/](https://www.ionos.fr/digitalguide/sites-internet/creation-de-sites-internet/tutoriel-json-ld-dapres-schemaorg/)
       * [https://developers.google.com/search/docs/guides/intro-structured-data?hl=fr](https://developers.google.com/search/docs/guides/intro-structured-data?hl=fr)
       * LOV Cloud and co : [https://miranda-zhang.github.io/cloud-computing-schema/v1.0/index-en.html#CloudService](https://miranda-zhang.github.io/cloud-computing-schema/v1.0/index-en.html#CloudService)
       * [https://miranda-zhang.github.io/cloud-computing-schema/v1.0/index-en.html#CloudService](https://miranda-zhang.github.io/cloud-computing-schema/v1.0/index-en.html#CloudService)
   * ontologie :
       * [http://cookingbigdata.com/linkeddata/dmservices/](http://cookingbigdata.com/linkeddata/dmservices/)


## Visio 2020.06.11

Proposition d'action :

   * Attendu qu'on part sur JSON-LD, tester une implémentation du JSON-LD sur le contenu des plateformes du PoC


## Visio 2020.06.15

Proposition :

   *     on part sur un poc minimaliste afin de vérifier que l'on peut couvrir l'ensemble de la chaine technique (oad, visu, ...)
   * réfléchir à la structuration des critères au format objet. Les objets devenant des @context dans les fichiers json-ld