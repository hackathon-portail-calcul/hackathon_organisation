
# Grille auto-organisation Hackathon PCSC

# Axe Moteur OAD



### Nom du groupe : OAD

## 

### Prochaine Visio : 16/06/2020 à 17h => réflexion moteur OAD + réflexion format fichier <=



## Membres inscrits

*Lister les membres et se présenter*

   * Jean-François Rey @INRAE Avignon DevOps
       * ce qui me plaît : Dev carte interactive
       * noob en ressources calcul/data et OAD
   * Olivier Porte : CNRS
   * Florian Trincal INRAE, DSI 
   * alexandre Dehne Garcia (INRAE, ancien adminsys cluster et stocage + dev (python, je déteste les techno web))
       *  en support sur les discussions et recherche de compétences. 
       * j'aurai peu de temps pour le développement :-(
   * Richard Randriatoamanana, @CNRS, cellule soutien à la recherche au labo des sciences du numérique de Nantes (LS2N) sur le site de l'Ecole Centrale de Nantes
       * en charge de mutualiser les ressources calcul/data en local et d'accompagner les projets et les users dans une utilisation optimisée et éclairée des services cloud mis à leur disposition dans l'éco-système local et ESRI
       * intéressé à réflechir/développer la partie moteur plutôt que la partie portail web side, mais je m'adapte toujours si besoin :-)




## Propositions / Idées



Il y a déjà cela : [https://hackathon-portail-calcul.pages.mia.inra.fr/existant/oad/](https://hackathon-portail-calcul.pages.mia.inra.fr/existant/oad/)



Ce n'est pas figé dans le marbre, ajouter, modifier, supprimer des infos comme bon vous semble !



~~Réaliser une page (ou site) static, avec une carte interactive filtré avec des indicateurs (CPU, RAM, flops, type de service, zone géographique...) => on peut s'inspirer de ce qu'il se fait côté page tech par mésocentre ~~[https://calcul.math.cnrs.fr/pages/mesocentres\_en\_france.html](~~https://calcul.math.cnrs.fr/pages/mesocentres\_en\_france.html~~)

~~Avec Hugo (pas testé encore) et leaflet via l’intégration et les pages GitLab.~~

~~Format des données en JSON regroupant les portails/solutions avec leurs indicateurs (avec un trie automatique des indicateurs qui viennent de fichier de données extérieur ou via l'API).~~

~~Via l'API cela permettrait d'avoir des données homogène (plus facile à traiter).~~

=> Cela a déjà été réaliser en partie dans le projet : [https://forgemia.inra.fr/hackathon-portail-calcul/existant](https://forgemia.inra.fr/hackathon-portail-calcul/existant)

Le moteur OAD doit être agnostique des API implémentés par les sites et pouvoir lire de manière asynchrone les données "poussées" par ces derniers; un format d'échange est primordial. On pourrait s'inspirer quelque chose comme le NRPE de NAGIOS.

Dans le meilleur des mondes, l'équipe **API** et **Formats** nous fournissent toutes ces informations bien formatées et complètes. 



[]Je ne sais pas où mettre ce point []: il ne suffit pas que l'utilisateur trouve le site qui lui convient, encore faut-il qu'il soit disponible. Il manque à mon avis la remontée de la charge des sites qu'il faut croiser avec les demandes pour aboutir à une proposition de site et d'agenda. Un beau sujet de RO ...

Il faudrait donc : 

    1/ développer une API donnant la charge des sites. Au choix, chaque site implémente l'API ou chaque site fournit les infos en asynchrone

    2/ faire saisir à l'utilisateur son besoin (c'est bien parti !)

    3/ rechercher la solution optimale ou, à défaut, une solution "la moins mauvaise". Cela implique de demander à l'utilisateur de renseigner ce qui est indispensable et ce qui peut être ajusté au cas où la solution optimale n'est pas disponible. Idem, la notion d'agenda doit être intégrée. Il faut aussi, sans doute, intégrer les notions de logiciels et de versions disponibles ...

    

## Rôles

Un rôle correspond à une “casquette”, chacun peut changer de casquette ou en porter plusieurs.

Vous pouvez changer les noms des rôles - A vos bonnes idées....

   * ***Huggy les bon tuyaux**** : chargé(s) de faire le lien avec l’équipe d’organisation et les autres groupes*
   * *Rôles spécifiques aux réunions  (rôles tournant)*
       * ***scribes**** : chargé de prendre les notes pendant les réunions*
       * ***cadenceurs**** : chargé de gérer le temps pendant les réunions*
   * ***La Balance**** : présente les résultats du groupe en plénière —> penser à le nommer quand un retour du groupe en plénière est annoncé*
   * ***Le Boulet**** : rappelle que ça doit être adapté à tous les types d’utilisateurs*
   * ***Dirty Harry**** : s’assure que tout avance, que les membres ne sont pas perdus*


Pleinière 5/06 : Olivier

Pleniere semaine suivante : Jean-François





## Cadrage (léger)

   * **1/ Apprentissage automatique de l'OAD**
       * Proposition suivant les données en entrée
   * **2/ Interface libre de recherche + carte interactive**
       * Afficher la carte en fonction des critères de recherche
   * Objectifs du groupe : **Définir comment l'utilisateur va faire son choix ou comment on va orienter l'utilisateur** (moteur OAD)
       * []Quelles sont les critères / indicateurs et comment on les obtient ?[]
           * Via un apprentissage de l'OAD grâce aux catégories/critères/indicateurs dynamiques
           * l'équipe **Critère, Format** nous le dit ?
           * l'équipe **API** nous le dit ?
           * A nous de définir les notre ?
           * on utilise un "standard" ?
       * []Comment on navigue parmi tous les critères ? []
           * Via un simple filtre : avec les critères (indicateurs) ?
           * Via des catégories (noob, avancé) et questions/réponses (qui peuvent modifier le simple filtre si dessus ?) ?
           * l'équipe **UX** a peut être une idée ?
               * point de vue utilisateur : est-ce que ressources disponibles et où ? réalité de la charge des sites ?
Acceptation d'un mode dégradé ? 



   * []critères / indicateurs :[]
       * Nom Portail / Site
       * Ville / geolocalisation (pour la carte)
       * Disponibilité (charge, temps d'attentes, durée possible des calculs/stockages...)
       * CPU / RAM
       * FLOPS
       * Espace stockage
       * mode dégradé
       * logiciels disponibles (et versions)
       * technologies
       * Prix / gratuité
       * a compléter


- Penser aussi au ressenti utilisateur pour que le formulaire soit suffisament vulgariser (expliquer) pour ne pas le perdre dès le début 



Intégrer un mode de recherche libre (chercher ou filtrer selon des critères) et une carte interactive

Apprentissage OAD : selon infos remontant des portails on peut décider des critères automatiquement --> le groupe critère fournit les infos, le groupe OAD tape dans l'API, apprentissage à faire

Outil d'aide à a décision sur les critères des différentes plateformes

Formulaire de contact renvoie direct au gestionnaire de plateforme / validation dans le PCSC --> moteur workflow ?

API est interrogée et fait le calcul

Etre modulaire pour que ça puisse être repris



A faire :

   * Lister besoins
   * Etablir la liste des plateformes existantes :
       * [https://staging.gaia-x-demonstrator.eu/](https://staging.gaia-x-demonstrator.eu/)
       * [https://www.edari.fr/](https://www.edari.fr/)
       * [https://choosealicense.com/](https://choosealicense.com/)
       * [https://calcul.math.cnrs.fr/pages/mesocentres\_en\_france.html](https://calcul.math.cnrs.fr/pages/mesocentres\_en\_france.html)
       * 





   * Liens avec autre groupe de travail ? Coordinations éventuelles 
       * **API**
       * **UX**
       * C**ritères**
       * **Format** 
       * *(et il en découle peut être les groupes Ontologies et Formats des Données)*
   * Moyens nécessaires (temps, outils)
3 personnes mais c'est limite en nombre

   * Comment vous décidez ? 
       * Décisions dans le groupe
       * Décisions en lien avec les autres ? (prendre contact avec les autres groupes après la plénière)


### Quels sont les Risques ===> les Solutions possibles correspondantes :

*risque x ===> solution de x *   

Beaucoup => Café +1    

 

 

 Utilisation de Hugo => Demander si qqun utilise déjà

 Utilisation de vuejs => Demander si qqun utilise déjà

 

 

 

 

 

 

 

 

   * Plan d’action / tâches / affecté à qui ? —> évolutif dans le temps, on se détend...
Faire au plus simple pour commencer et monter brique par brique mais toujours avec qqch qui fonctionne.

Via les Issues dans Gitlab / Bug / fonctionnalités ...



       * JF Rey
           *  => Dev intégration d'une carte interactive sur le projet existant [https://hackathon-portail-calcul.pages.mia.inra.fr/existant/oad/](https://hackathon-portail-calcul.pages.mia.inra.fr/existant/oad/) 
           * Code sur le gitlab (fork [https://forgemia.inra.fr/jean-francois.rey/existant)](https://forgemia.inra.fr/jean-francois.rey/existant)) et générer un site statique avec hugo et VueJS (Vue2leaflet) [https://jean-francois.rey.pages.mia.inra.fr/existant/oad/](https://jean-francois.rey.pages.mia.inra.fr/existant/oad/)
## 


