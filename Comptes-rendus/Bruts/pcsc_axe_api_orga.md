
# Grille auto-organisation Hackathon PCSC

# Axe API



### Nom du groupe : 

## 



## Membres inscrits

*Lister les membres et se présenter*

   * Arnaud Jean-Charles
   * Richard RANDRIATOAMANANA, CNRS, LS2N UMR sur le site Ecole Centrale de Nantes
   * alexandre Dehne Garcia
   * Tovo Rabemanantsoa
   * Pierre-Emmanuel GUERIN, Ecole Centrale de Nantes, Mesocentre ICI-CNSC






## Rôles

Un rôle correspond à une “casquette”, chacun peut changer de casquette ou en porter plusieurs.

Vous pouvez changer les noms des rôles - A vos bonnes idées....

   * ***Huggy les bons tuyaux**** : chargé(s) de faire le lien avec l’équipe d’organisation et les autres groupes*
   * *Rôles spécifiques aux réunions  (rôles tournant)*
       * ***scribes**** : chargé de prendre les notes pendant les réunions*
       * ***cadenceurs**** : chargé de gérer le temps pendant les réunions*
   * ***La Balance**** : présente les résultats du groupe en plénière —> penser à le nommer quand un retour du groupe en plénière est annoncé*
   * ***Le Boulet**** : rappelle que ça doit être adapté à tous les types d’utilisateurs*
   * ***Dirty Harry**** : s’assure que tout avance, que les membres ne sont pas perdus*
## 



## Cadrage (léger)

   * Objectifs du groupe
       * choisir entre SOAP et REST
       * sur un use case très simple (get/post/put d'une info simple (exemple version d'un logiciel) , construire une demo simple :
           * périmètre de l'API
           * décrire ce que fait l"API
           * définir la syntaxe de l'API
           * monter la solution technique du moteur de l'API
               * trouver la donnée et la renvoyer
               * pousser la donnée, mettre à jour le json qui va bien, commit \& push GIT dans la foulée
   * Liens avec autre groupe de travail ? Coordinations éventuelles
       * OAD, l'API interroge l'OAD
       * Ontologie pour intégrer l'ontologie qui va bien avec le use case dans les requête et réponses
   * Moyens nécessaires (temps, outils)
   * Risques
   * Évaluation des risques


### Quels sont les Risques ===> les Solutions possibles correspondantes :

*risque x ===> solution de x *   

mal documenter la doc, API pas utilisée ==> documenter au fur et à mesure     

 

###  IDEES :

Intégrer tous les critères sans pour autant rendre complexe l'API



   * Pas session
   * 

Les API REST imitent la façon dont le web lui-même marche dans les échanges entre un client et un serveur. Une API REST est :

   * Sans état
   * Cacheable (avec cache = mémoire)
   * Orienté client-serveur
   * Avec une interface uniforme
   * Avec un système de couche
   * Un code à la demande (optionnel)


POST/PUT des fournisseurs de ressources vers PCSC

   * permettre à des sondes d'envoyer des infos vers PCSC via l'API
       * le push mettrait à jour le fichier JSON du fournisseurs via un commit/push git
       * adapter l'API aux sondes les plus communes 
           * NAGIOS
       * use case : mise à jour automatique du dernier numéro de version d'un soft (PUT scripté en fin de script d'install du soft)
       * pas session mais le passage d'une clef d'identification 


GET depuis PCSC

   * requêtes d'extraction d'infos 
   * requêtes d'infos générale (list des ressources avec plus de 10000 coeurs)
   * GET avec JSON USER + BESOIN ou que par API ?
   * retour en JSON 
   * Requête std : 
   * Je veux tel  critère (<>=,bool) avec la possibilité de chainer les critères + en options [–]
       * => renvoie une liste d'identifiants 
       * => renvoie tous les tous les fichiers json complet de la ressource
       * => renvoie que les champs spécifiquement demandé dans la requête (pas les champs de sélection)




Method REST API : GET, HEAD, POST, PUT, PATCH, DELETE, CONNECT, OPTIONS et TRACE

ou 

SOAP WSDL 

ou 

les 2 ???





   * se baser sur openAPI ?
Exemple d'API (bonnes):

    [https://api.archives-ouvertes.fr/docs](https://api.archives-ouvertes.fr/docs)

    [http://api.bnf.fr/accueil](http://api.bnf.fr/accueil)

    [https://geonetwork-opensource.org/manuals/trunk/en/api/index.html](https://geonetwork-opensource.org/manuals/trunk/en/api/index.html)  (bonne doc)

    [http://www.markus-lanthaler.com/research/on-using-json-ld-to-create-evolvable-restful-services.pdf](http://www.markus-lanthaler.com/research/on-using-json-ld-to-create-evolvable-restful-services.pdf)

 

 

 Essayer de rester dans HUGO !!! mais si besoin mettre en place un serveur REST

 LIB javascript REST : [https://marmelab.com/blog/2015/03/10/deal-easily-with-your-rest-api-using-restful-js.html](https://marmelab.com/blog/2015/03/10/deal-easily-with-your-rest-api-using-restful-js.html)

 

 Bonnes pratiques : [https://guide-api-rest.marmicode.fr/](https://guide-api-rest.marmicode.fr/)

 



 

   * Plan d’action / tâches / affecté à qui ? —> évolutif dans le temps, on se détend...
       * trouver de bons exemple d'API > [https://docs.gitlab.com/ee/api/](https://docs.gitlab.com/ee/api/)
       * tester chacun de son côté une API via gitlab-api ou javascript sur le repo
       * une spécification pour construire des API au format JSON [https://jsonapi.org/](https://jsonapi.org/) (talk de Gebhardt [https://static.sched.com/hosted\_files/asc2019/1f/Intro%20to%20JSONAPI.pdf)](https://static.sched.com/hosted\_files/asc2019/1f/Intro%20to%20JSONAPI.pdf))
       * chercher d'autres standard que openAPI ou de bonnes pratiques
           * [https://github.com/OAI/OpenAPI-Specification](https://github.com/OAI/OpenAPI-Specification)
           * [https://openapis.org/](https://openapis.org/)
           * [https://swagger.io/specification/](https://swagger.io/specification/)
           * [https://hackernoon.com/restful-api-designing-guidelines-the-best-practices-60e1d954e7c9](https://hackernoon.com/restful-api-designing-guidelines-the-best-practices-60e1d954e7c9)
       * API spécification conf en 2019 [https://asc2019.sched.com/](https://asc2019.sched.com/)
       * [http://apistylebook.com/](http://apistylebook.com/)
       * [https://cloud.google.com/apis/design/resources#design\_flow](https://cloud.google.com/apis/design/resources#design\_flow)
       * MOOC sur API [https://openclassrooms.com/en/courses/3449001-utilisez-des-api-rest-dans-vos-projets-web/3449008-quest-ce-quune-api](https://openclassrooms.com/en/courses/3449001-utilisez-des-api-rest-dans-vos-projets-web/3449008-quest-ce-quune-api)
       * exemple d'APIs 
           * PayPal [https://developer.paypal.com/docs/api/overview/](https://developer.paypal.com/docs/api/overview/)
           * GMAIL [https://developers.google.com/gmail/api/](https://developers.google.com/gmail/api/)
       * **editeur API sympa** [http://editor.swagger.io/](http://editor.swagger.io/)


   * Objectif de l'hackathon
    - POC ultra minimaliste mais fonctionnel basé le maquette actuelle et en appliquant les bonnes les bonnes pratiques et avec une description de type OpenAPi

   *     - requête sur seulement 2 critères combinables avec la possibilité de spécifier 2 champs
       * => renvoie une liste d'identifiants des ressources qui matchent
       * => renvoie tous les tous les fichiers json complet des ressources qui matchent
       * => renvoie que les champs spécifiquement demandé dans la requête (pas les champs de sélection)
   **Etapes pour arriver à l'objectif :**

       1. écrire l'url possible de la requête 
       1. décrire la description du service
       1. décrire la réponse
       1. trouver un moteur server-web API
       1. implémenter


Code du FORK : [https://forgemia.inra.fr/hackathon-portail-calcul/pcsc\_api](https://forgemia.inra.fr/hackathon-portail-calcul/pcsc\_api)



### **Point 0.1**

url du type : [https://api.pcs.fr/search?q=](https://api.pcs.fr/search?q=)

nom : ressource / valeurs : calcul, stockage



exemple interrogation avec la méthode GET

    GET /<version>/<path\_of\_method>/search?<variable>=<value>\&<variable>=<value>

GET /<version>/ressources -> renvoie la liste identifiants des ressources

GET /<version>/ressources/id23 -> renvoie la ressources de l'identifiant id23





au niveau sécurité, on pourrait avoir comme HTML headers

    "Accept: application/<format>" format={json,xml,csv,rss} 

    "Authorization: Bearer <access-token>" access-token = il doit être unique à chaque user autorisé et connu



ou sinon simplement du genre

    /search?access\_token=ACCESS\_TOKEN

    

exemple d'API PCS sur une interrogation sur les mesocentres en Gironde qui propose un FS GPFS 

    GET /v1/platform/search?type=mesocentre\&storagetyoe=GPFS\&location=gironde



### **Point 0.2**

Description de l'API dans le standard openapi

[http://spec.openapis.org/oas/v3.0.3](http://spec.openapis.org/oas/v3.0.3)



Exemple de format OpenAPI pour un objet type information   

    {

      "title": "PCSC",

      "description": "Portail Calcul, Stockage et Cloud"

      "termsOfService": "[http://api.pcs.fr/terms/](http://api.pcs.fr/terms/)",

      "contact": {

        "name": "API Support",

        "url": "[http://api.pcs.fr/support](http://api.pcs.fr/support)",

        "email": "support@pcs.fr"

      },

      "license": {

        "name": "Apache 2.0",

        "url": "[https://www.apache.org/licenses/LICENSE-2.0.html](https://www.apache.org/licenses/LICENSE-2.0.html)"

      },

      "version": "1.0.1"

    }



Une autre proposition de format d'appelle

curl -X POST -u "apikey:{APIKEY}" --header "Accept: application/json " \

[https://api.pcs.fr/instances/hashkey\_file](https://api.pcs.fr/instances/hashkey\_file)



### **Point 0.3**

format json par défaut du type : 

        {

            "response":

                "numFound": <int>,

                "docs": [

                    {

                        "pcsid": <int>,

                        ...

                    },

                    {...},

                    ...

                ]

        }

    

Si erreur dans la requête ou la réponse :

        {

            "error":

                "metadata": {

                    "msg": <str>,

                    "code": <str>

                }

        }

   * 

**Specs OpenAPI**

exemple d'un object PATH et une méthode GET

    **/platforms**:

      **get**:

        description: renvois toutes les informations sur les plateformes que le demandeur a accès

        **responses**:

          '200':

            **description**: liste les plateformes 

            **content**:

              application/json:

                schema:

                  type: array

                  items:

                    $ref: '#/components/schemas/platforms'



### **Point 0.4**

Des moteurs comme

   * AWS 
Juste pour exemple : [https://realpython.com/flask-connexion-rest-api-part-2/](https://realpython.com/flask-connexion-rest-api-part-2/)



 

### **Point 0.5**

format json par défaut du type : 



## Besoin de ressources/compétences :

   * - Nécessité d'avoir un serveur tierce pour implémenter l'API <- Alex
   * - Développement côté serveur à faire
   * - Spécifications de l'API de base à définir <- Arnaud et Pierre-Emmanuel
   * - [http://editor.swagger.io/](http://editor.swagger.io/)