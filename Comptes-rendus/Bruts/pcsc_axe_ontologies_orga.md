
# Grille auto-organisation Hackathon PCSC

# Axe Ontologies



### Nom du groupe : 

## 



## Membres inscrits

*Lister les membres et se présenter*

   * Cyril Jousse (Univ Clermont Auvergne, interface chimie-biologie, métabolomique)
       * Lien groupe Doc
   * Sophie Aubin (INRAE, Direction pour la Science Ouverte, sémantique)
       * Dirty Harry
   * Arnaud Jean-Charles (CNRS, LabEx DRIIHM, projet sur la gestion des données)
       * Lien groupe format
   * alexandre Dehne Garcia (orga observateur)


## Rôles

Un rôle correspond à une “casquette”, chacun peut changer de casquette ou en porter plusieurs.

Vous pouvez changer les noms des rôles - A vos bonnes idées....

   * ***Huggy les bons tuyaux**** : chargé(s) de faire le lien avec l’équipe d’organisation et les autres groupes*
   * *Rôles spécifiques aux réunions  (rôles tournant)*
       * ***scribes**** : chargé de prendre les notes pendant les réunions*
       * ***cadenceurs**** : chargé de gérer le temps pendant les réunions*
   * ***La Balance**** : présente les résultats du groupe en plénière —> penser à le nommer quand un retour du groupe en plénière est annoncé*
       * 5/06 Arnaud
   * ***Le Boulet**** : rappelle que ça doit être adapté à tous les types d’utilisateurs *
   * ***Dirty Harry**** : s’assure que tout avance, que les membres ne sont pas perdus - Sophie*


## Cadrage (léger)

   * Objectifs du groupe :  s'appuyer sur des définitions explicites et univoques des notions et objets manipulés dans le portail 
Interface 

Niveau humain : première étape : clarifier notions abordées (documentation)

Niveau : construire ontologie pour structurer la base -  information interprétables par des programmes / exposition Json --> peut-être pas faisable dans le cadre du hackathon []à voir ensuite ?[]



Définir une arborescence / décrire infra

Des ressources existent --> Identifier les ontologies sur lesquelles on peut s'appuyer

Référents : comment on utilise une ontologie et définitions dans la doc.



Peut-être besoin de deux niveaux de définitions (moldus / informaticiens)



choix d'arborescences de vocabulaires et notions; accessible à tous.

   * Liens avec autre groupe de travail ? Coordinations éventuelles 
       * groupe critères : approche métier --> vont donner des définitions pour aller chercher les classes qui vont bien dans les ontologies existantes
       * groupe UX : ce qu'on va montrer et à qui --> scénarios d'utilisation
       * groupe documentation : info utiles utilisateurs
       * groupe formats : spécification du fichier json (schéma)
           * interaction sur où on met une ontologie dans un schéma de données souple et évolutif
   * Moyens nécessaires (temps, outils) : 
       * benchmarking sur solutions disponibles (mais moins bien !!) ?
       * outil pour échanger les définitions avec les autres groupes : interface (la DipSO peut fournir un outil)


Attentes des autres groupes (réunion du 08/06)

- Axe Critères : définition du périmètre Service/Ressources/Utilisateurs (voir doc [https://etherpad.in2p3.fr/p/pcsc\_axe\_criteres)](https://etherpad.in2p3.fr/p/pcsc\_axe\_criteres))  --> discussions à prévoir Ontologies / Critères 

- Axe Formats : 

   *     - questionnement sur le passage de JSON à JSON-LD (un avis à émettre par notre groupe ?) 
           * - Avantages : (Arnaud) intéressant en terme de représentation pour définir le vocabulaire utilisé par le format Json /  (Tovo) pour valider les informations en back-end / (Alex) permet d'ajouter des métadonnées de contexte aux informations fournies / (Arnaud) intéressant surtout pour le schéma, pas forcément pour les instances de la base
           * - Limites: (Alex) il ne semble exister qu'une seule librairie pour exploiter json-ld
       * - possibilité d'intégrer les informations sémantiques dans la page web avec RDFa (exploité par google notamment via schema.org)
       * --> besoin d'une discussion sur le JSON-LD avec le groupe Formats


Tester la liste de Critères par divers ontologies disponibles (3-4 max)

       * --> faire des requêtes sur des serveurs de concepts ou des entrepôts pour chaque critère, par ex sur [https://lov.linkeddata.es/dataset/lov/api](https://lov.linkeddata.es/dataset/lov/api)  [https://lovinra.inrae.fr/](https://lovinra.inrae.fr/)
       * 

### Quels sont les Risques ===> les Solutions possibles correspondantes :

*risque x ===> solution de x *   

    - s'embarquer dans la construction d'une ontologie ad hoc ou trop complexe

 



 

   * Plan d’action / tâches / affecté à qui ? —> évolutif dans le temps, on se détend...
   * Réunion : lundi 8/06 13h30-15h [https://meet.jit.si/pcsc\_ontologies](https://meet.jit.si/pcsc\_ontologies)
   * Identifier des ontologies qui seraient réutilisables pour le projet : débroussailler
       * Clarifier les notions propres au projet
       * Cooking big data (ontologie cloud) : [http://cookingbigdata.com/linkeddata/dmservices/](http://cookingbigdata.com/linkeddata/dmservices/)
       * cocoon : [https://miranda-zhang.github.io/cloud-computing-schema/v1.0/index-en.html](https://miranda-zhang.github.io/cloud-computing-schema/v1.0/index-en.html) 
       * il y a peut-être à regarder de ce côté là : [https://cedar.metadatacenter.org](https://cedar.metadatacenter.org) 
       * DbPedia 
       * ontologie généraliste de computer science avec des entrées pertinentes et des définitions : [https://cso.kmi.open.ac.uk/home](https://cso.kmi.open.ac.uk/home)
       * PROV-O [https://www.w3.org/TR/prov-o/](https://www.w3.org/TR/prov-o/) .  [https://www.canal-u.tv/video/inria/provenance\_et\_tracabilite.20407](https://www.canal-u.tv/video/inria/provenance\_et\_tracabilite.20407) COnseils de Catherine Roussey (TSCF)
           * ontologie prov avec des activités et des agents est un bon candidat. les services info sont les agents qui execute une activité ( tout traitement informatique : calcul ou indexation pour le stockage etc...).
           * perso je prendrais une ontologie generique du genre prov et je remplirais les roles des agents avec l'ontologie (qui a mon avis a plus la forme d'un thesaurus thematique) [https://cso.kmi.open.ac.uk/home](https://cso.kmi.open.ac.uk/home)
           * il ne faut pas chercher une ontologie de service, il faut chercher comment prov est utilisé pour decrire des services. dans research objet prov est utilisé pour decrire les resultats de la recherche donc normalement ca doit etre compatible avec la description d'un service. un service prend des données en entrée et fournit des données en sortie.
           * l'ontologie à concevoir doit etre une spécialisation de prov.
   * Faire lien avec les groupes critères et formats
       * schéma conceptuel des données : [https://forgemia.inra.fr/hackathon-portail-calcul/hackathon\_organisation/-/blob/master/Crit%C3%A8res/criteres.svg](https://forgemia.inra.fr/hackathon-portail-calcul/hackathon\_organisation/-/blob/master/Crit%C3%A8res/criteres.svg) 
       * Réunion à organiser autour de JSON-LD et autres sujets avec Formats
   * travail sur la base du json test ? 
   * Trouver des spécialistes d'ontologies infra-informatiques
       *  --> INRIA ? -> Sophie demande dans son réseau (voir plus haut)
       * envoyer un message à web-semantique web.semantique@lists-sop.inria.fr (milieu de semaine 3)
Lien vers un test sur web protégé : [https://webprotege.stanford.edu/#projects/9567749d-9ea7-411c-bb63-67c6c009c15d/edit/Classes?selection=Class(%3Chttp://cookingbigdata.com/linkeddata/ccinstances%23ram%3E)](https://webprotege.stanford.edu/#projects/9567749d-9ea7-411c-bb63-67c6c009c15d/edit/Classes?selection=Class(%3Chttp://cookingbigdata.com/linkeddata/ccinstances%23ram%3E)) Il faut créer un compte

Liste des ontologies utiles : 

    - SKOS : [https://www.w3.org/TR/2005/WD-swbp-skos-core-guide-20051102/](https://www.w3.org/TR/2005/WD-swbp-skos-core-guide-20051102/) (relations entre classes)

    - schema : [http://schema.org/](http://schema.org/) (organismes, localisation)

    - foaf : [http://xmlns.com/foaf/0.1/](http://xmlns.com/foaf/0.1/) (personnes)

    - [https://miranda-zhang.github.io/cloud-computing-schema/v1.0/index-en.html#CloudService](https://miranda-zhang.github.io/cloud-computing-schema/v1.0/index-en.html#CloudService)

    

##     Réunion mercredi 10/06 10h :

        Ontologies cloud : cookingbigdata

        Ontologies  pour les personnes : FOAF

        Ontologies pour utilisateurs : ? se rapprocher de UX ?

        Champs fournisseurs et utilisateurs en commun ?

        Décrire la partie ressource ? Complexe

        Partir d'un exemple d'un fournisseur pour créer des ontologies.

        

        Exemple : [http://fr.dbpedia.org/page/M%C3%A9moire\_vive](http://fr.dbpedia.org/page/M%C3%A9moire\_vive)

        Format : JSON-LD; [https://json-ld.org/playground/](https://json-ld.org/playground/)

        Découverte de [https://webprotege.stanford.edu](https://webprotege.stanford.edu)

        > proposition d'avancer sur deux axes, outils et méthodologies

        Champs d'exploration : CPU et Utilisateur

   *         Sophie fait des tests dans VocBench
   * Description de l'ontologie cookingbigdata : [https://arxiv.org/pdf/1806.06826.pdf](https://arxiv.org/pdf/1806.06826.pdf)
   * Schéma de Ontology for Cloud Computing instances de cookingBigData : [http://cookingbigdata.com/linkeddata/ccinstances/documentation/#overv](http://cookingbigdata.com/linkeddata/ccinstances/documentation/#overv)
   * Exemple de DBpedia : [http://fr.dbpedia.org/page/M%C3%A9moire\_vive](http://fr.dbpedia.org/page/M%C3%A9moire\_vive)
   * Recherche dans le Linked Open Data Cloud : [https://lod-cloud.net/datasets](https://lod-cloud.net/datasets)
   * 

   * Réunion lundi 15/06 11h
   * - Retour du groupe Formats : 
       * -json-ld permet de rattacher les objets à une définition  sémantique
       * - Json-ld : POC avec description des quelques éléments pour tester la conservation des fonctionnalités de l'OAD
       * - qui fait le schéma de données ? 
       * - groupes format / critères fournissent les objets à définir
       * - groupe ontologie : responsable de la sémantique
   * - Outils 
       * - VocBench [https://vocbench.irstea.fr/vocbench3](https://vocbench.irstea.fr/vocbench3) (visiteur@inrae.fr / visiteur)
       * - Webprotégé : [https://webprotege.stanford.edu](https://webprotege.stanford.edu)
   * -  l'ontologie permettra de gérer la définition formelle des "termes"  et les listes de valeurs possibles pour les formulaires de saisie
   * - Suite : 
       * - réunion du format mercredi à 13h
       * - Ontologie continue à tester les outils sur 2 objets (CPU/Utilisateur) / Sophie teste l'approche avec l'ontologie PROV
       * - quand le POC sera avancé, on verra comment ça se rejoint
   * TODO : Continuer à tester les outils avec les deux exemples (; 
   * - Prochaine réunion mercredi 17 à 14h