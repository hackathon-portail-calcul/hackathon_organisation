


# Grille auto-organisation Hackathon PCSC

# Axe User eXperience



### Nom du groupe : 

## 



## Membres inscrits

*Lister les membres et se présenter*

   * Eric Cahuzac (INRAE, Pôle Num4Sci Toulouse)
   * Fred de Lamotte (INRAe - Montpellier)
   * Nadia Ponts (INRAE - Bx)
   * Cyril Jousse (Univ Clermont Auvergne)
   * Alexandre Dehne Garcia (INRAE)
   * Emilie Lerigoleur (CNRS - UMR GEODE Toulouse)
   * Gilles Mathieu (Inserm DSI - Lyon)
   * Daniel Salas (Inserm DSI - Paris)
   * Oana Vigy (CNRS, Montpellier)






## Rôles

Un rôle correspond à une “casquette”, chacun peut changer de casquette ou en porter plusieurs.

Vous pouvez changer les noms des rôles - A vos bonnes idées....

   * ***Huggy les bons tuyaux**** : chargé(s) de faire le lien avec l’équipe d’organisation et les autres groupes*
       * alex + 
   * *Rôles spécifiques aux réunions  (rôles tournant)*
       * ***scribes**** : chargé de prendre les notes pendant les réunions*
       * ***cadenceurs**** : chargé de gérer le temps pendant les réunions*
   * ***La Balance**** : présente les résultats du groupe en plénière —> penser à le nommer quand un retour du groupe en plénière est annoncé*
       * Eric
   * ***Le Boulet**** : rappelle que ça doit être adapté à tous les types d’utilisateurs*
       * Nadia
   * ***Dirty Harry**** : s’assure que tout avance, que les membres ne sont pas perdus*
       * Gilles
## 

Enjeu: S'assurer que le portail réponde aux attentes des utilisateurs

Idées: 

   * épuré au max,
   * léger (chargement rapide)
   * responsive (tablette, téléphone, ...)
   * joli
   * accompagnant sans être harcelant (pas de pop-up, de propositions d'aide dans tous les sens )
   * lister les sites inspirants et les sites rebutants
   * bilingue (français + anglais) cf ontologie mot simple en fr -> mot standardisé -> mot simple en anglais
   * aide depuis l'oad 
   * effort constant de se mettre à la place du user
   * est-ce important d'être très précis sur le besoin du user ?
   * vidéo explicative sur l'usage du portail (se baser sur des uses cases)
   * proposer une liste de formations (ou pointer vers des référenceurs de licences)
   * construire un parcours de formation
   * modulariser le site et retenir les fonctionnalités choisies (cf risques)
   * faire des ateliers de prototypage




## Cadrage (léger)



   * Objectifs du groupe
       * proposer des scénarii d'utilisation
       * dessiner l'interface telle qu'on l'imagine
       * Périmètres : 
           * Page accueil
           * OAD
           * DOC
       * 

   * S'assurer que l'interface :
       * exploite les critères définis (par le groupe "critères")
       * réponde aux exigences des utilisateurs
           * exemple 
               * pas forcément de compte/mot de passe pour utiliser le portail
               * moyen de contacter le portail si pb
       * soit ergonomique, intuitive, rapide-efficace, adaptative-évolutive
       * adapté aux environnements utilisateurs (navigateurs, mobile...)
       * permette d'exporter les résultats de la recherche (formats différents?)
       * 

   * Liens avec autre groupe de travail ? Coordinations éventuelles
       * critères
       * chatbot
       * OAD
       * documentation
       * API?
   * Moyens nécessaires (temps, outils)
       * faire appel à des ressources ext 
       * Recensement besoins et usages par une enquête aux participants du hackathon --> groupe doc intéressé aussi
       * Besoin compétences 
           * design web : outils dessiner use cases
           * soumission de use cases
   * Risques


### Quels sont les Risques ===> les Solutions possibles correspondantes :

*risque x ===> solution de x *   

interface non-intuitive (inutilisable dès le début). ===> personne ne s'en servira (et ne reviendra pas)

interface non-adaptative ===> impossible d'adapter aux besoins/retour utilisateurs ===> utiliser une source ouverte facilement modifiable par personne "lambda" pas oblig développeur   

interface trop chargée ===> fuite des newbies

mettre des cookies pour retenir les souhaits des users ==> peur du flicage

 

 

 

 

** LIstes des sites inspirants : **

   *  [https://ngphylogeny.fr/](https://ngphylogeny.fr/)
       * par défaut/advanced
       * clair, beau 
   * [https://www.gbif.org/fr/](https://www.gbif.org/fr/)
       * épuré, clair, beau
       * menu How-to pas mal
   * [https://data.ifremer.fr/](https://data.ifremer.fr/)
       * mots "simples"
       * bilingue fr-en
   * [https://www.seadatanet.org/](https://www.seadatanet.org/) (dans le même genre que ci-dessus)
   * [https://doranum.fr/](https://doranum.fr/)
       * plus axé "accompagnement", peut donner des (bonnes) idées...
   * [https://www.edari.fr/](https://www.edari.fr/)
   * [https://www.casd.eu/](https://www.casd.eu/)
       * épure et version anglaise fonctionnelle!
   * [https://www.ouvrirlascience.fr/](https://www.ouvrirlascience.fr/)
       * pas pour ses couleurs (beurk) mais pour sa simplicité
   * [https://www.knime.com/](https://www.knime.com/)
       * pour la seconde page de la page d'accueil qui propose des cas d'usage
   * [https://uxdesign.cc/form-best-practices-8e560e9f8bd0](https://uxdesign.cc/form-best-practices-8e560e9f8bd0)
       * best practices en form design : bien fait avec des exemples visuels
 

**  LIstes des sites rebutants :**

   * [http://www.amelie.fr](http://www.amelie.fr)
   * [https://cat.opidor.fr/index.php/Cat\_OPIDoR,\_wiki\_des\_services\_d%C3%A9di%C3%A9s\_aux\_donn%C3%A9es\_de\_la\_recherche](https://cat.opidor.fr/index.php/Cat\_OPIDoR,\_wiki\_des\_services\_d%C3%A9di%C3%A9s\_aux\_donn%C3%A9es\_de\_la\_recherche)
       * exemple de catalogue de ressources sous la forme d'un wiki
       * un peu old school
   * [https://www.oca.eu/fr/services-et-equipements/guides-et-tutoriels/256-categorie-fr-fr/oca/bibfr-principal/1396-gerer-et-diffuser-les-donnees-de-la-recherche](https://www.oca.eu/fr/services-et-equipements/guides-et-tutoriels/256-categorie-fr-fr/oca/bibfr-principal/1396-gerer-et-diffuser-les-donnees-de-la-recherche)
       * trop d'infos tue l'info !
   * [https://illo.tv/tarotobot/](https://illo.tv/tarotobot/)
       * ça brille et ça pete!








 

   * Plan d’action / tâches / affecté à qui ? —> évolutif dans le temps, on se détend...
### @tous : 

   * - trouver au moins un site inspirant : Emilie = done ! Nadia = done !
   * - trouver au moins un use case basé sur du réel (stockage, calcul, cloud seuls ou combinés) : Emilie = done !
   * - interroger des collègues sur ce qu'ils aimerait : Emilie = done ! pas de retours pour le moment ; Nadia = pas de retour non plus (ça leur a semblé abstrait. tant qu'ils n'ont pas d'exemple concret)
   * - trouver au moins une qualité et un défaut la maquette : Emilie = done ! Nadia = done!
   * 

   * 

   * 

   * 

# Uses cases : 

    format libre

    #1

    #2

   

   Quelques idées en vrac :

   * je veux qu'on m'aide à trouver où stocker mes données volumineuses avec les meilleures garanties de sécurité et de durée
   * je veux qu'on m'aide à trouver où je peux lancer des calculs sur mes données (locales et/ou distantes) pour me faciliter la vie
   * je veux monter en compétences dans le domaine du calcul scientifique intense et qu'on m'aide à trouver la meilleure formation adaptée à mon besoin et mes contraintes (cf. [http://www.sup-numerique.gouv.fr/](http://www.sup-numerique.gouv.fr/) et [http://www.sup-numerique.gouv.fr/pid36561/catalogue-formations-distance.html](http://www.sup-numerique.gouv.fr/pid36561/catalogue-formations-distance.html) et [http://www.sup-numerique.gouv.fr/pid33135/moocs-calendrier-des-cours-en-ligne-ouverts-et-massifs.html)](http://www.sup-numerique.gouv.fr/pid33135/moocs-calendrier-des-cours-en-ligne-ouverts-et-massifs.html))
   * je veux que mes résultats de calculs (ou autres) soient partagés largement au sein de la communauté scientifique (science ouverte) et qu'on me recommande le meilleur entrepôt de données dédié à la diffusion de ces résultats [auto-commentaire : c'est peut-être un peu borderline, mais ça me semble important d'intégrer notre approche dans le cycle de vie complet des données]
  

Une requête reçue pour de vrai :

"We would need a ssh-accessible Ubuntu/Debian Virtual Machine with 12 cores / 64GBs RAM, a total of 5TB of storage attached to said Virtual Machine. For the first year we can start with 1TB of storage. "



une autre requête

"je traite des échantillons via CellRanger Count pour du Single Cell RNAseq. Cela me prend environ une semaine de calcul sur ma machine locale (bi Xeon E5-2687w-v3 - 128Go RAM). Quelles ressources je peux utiliser pour améliorer ce temps de calcul ?"



Une autre requête d'un collègue en SHS (plateforme alimentaire):

       * Pour chaque service, l'affichage clair de son prix, de ses capacités max (Go pour le cloud, mémoire pour le calcul, etc.) et ses "niveaux" de services (ex:  "redondé"/"non redondé", "crypté/...").
       * Un lien vers les technos mises en place (de quel cloud on parle), et celles compatibles avec le service (ex: protocole S3, NFS, etc.).
       * Un schéma rapide de la phase d'acquisition du service. Quelles sont les grandes étapes. Ariane est pénible pour ça. Quand on remplit un formulaire, on ne sait pas ce qui va se passer (remonté au DU, validation par 3 services, passage par le réseau, puis le gestionnaire AD, etc.).
       * Un lien vers le formulaire/la personne pour avoir plus de renseignements avant la demande, le demander, ou pour avoir de l'aide. Pour ce qui est de l'aide, définir les niveaux de responsabilité (c'est-à-dire qui contacter suivant les cas : Réseau si on veut le raccorder, gestionnaire du service s'il n'y a plus de place, etc.).


Une  autre vraie requête : 

    "je veux analyser mes SNP simplement sans me  soucier des quotas de stockage pendant mon calcul"



    

# Maquette actuelle :

    [https://ingenum.pages.mia.inra.fr/pcs/](https://ingenum.pages.mia.inra.fr/pcs/)

##     Qualités :

   * page accueil : 
       * épurée mais c'est normal car en construction ;)
       * deux gros boutons, 1 pour la doc, 1 pour l'outil = nickel
   * Documentation :
       * 

       * 

   * Outil d'aide à la décision :
       * semble responsive
       * épuré, ergonomique
       * menus déroulants aident bien à la saisie
       * l'aide sous la forme d'astérisque avec un "?" pour mieux comprendre un terme
       * efficace, répond vite 
   * 

   * Défauts : 
   * page accueil : 
       * manque de dynamisme du site: news et autre actu
       * 

   * Documentation :
       * en l'état, imbuvable !!! trop de texte, des définitions pas claires, qui fera l'effort d'aller dans un "dico" comme ça ?? désolée, je suis un peu sévère...
       * mal structurée
       * non ergonomique
       * trop figé sur le site
       * en français seulement alors que plein de termes sont souvent rencontrés/utilisés en anglais
       * 

   * Outil d'aide à la décision :
       * quelques vocabulaires incompréhensibles pour un béotien, ex : les types de stockage dans le menu déroulant !
       * manque des critères ? ex : coût ?
       * on ne sait pas ce qu'on cherche (calcul?Stockage?Cloud? les 3?)
       * C'est une interface expert, impossible de faire des requêtes simples et pragmatiques


## Améliorations possibles :

    

   * Outil d'aide à la décision :
       * rajouter la date de dernière mise à jour pour chaque datacenter dans le listing des centres de calcul ?
       * faut-il rajouter le critère "dans quel(s) objectif(s) ?" ex : stockage "simple", stockage+calcul ???
           * je pense que oui, pour développer une arborescence décisionnelle derrière


   * Documentation
       * le glossaire pourrait renvoyer vers des articles techniques développés sur d'autres sites?
           * ou a un fil de discussion matermost?
           * ou a des FAQ?




# Visio du 08/06



Daniel rejoint le groupe pour voir les interactions Ux:Chatbot



Cyril : Chatbot, pas une bête de course (chronophagie !!), pas pour infantiliser les utilisateurs.

Daniel: Chatbot, peut rendre le questionnaire plus vivant/aux listes déroulantes

Peut aider à la clarification et aux choix des termes/critères pour l'utilisateur.

Peut aider à spécifier l'utilisateur.



Le groupe Doc a déjà défini deux profils : **"utilisateur de plateforme" et "administrateur de plateforme qui affiche les infos sur PCSC".**

-> ? n'y aurait-il pas un 3ème profil : user support/aide, genre les gens qui font la recherche pour les utilisateurs ? Genre, des informaticiens qui ont un peu analysé le besoin utilisateur et cherchent les ressources pour ça...



Cyril : une fenêtre-lien comme wikipedia ? Alex : Une fenêtre en plus ? Pour de la persistance de l'info (des 3 dernières infos, ...).

Emilie : exemples avec [https://www.geoportail.gouv.fr/carte](https://www.geoportail.gouv.fr/carte) et cliquer sur le point d'interrogation; [http://www.globalcarbonatlas.org/en/CO2-emissions](http://www.globalcarbonatlas.org/en/CO2-emissions) bouton ? mais ici exemples "figés"



Il nous faut démarrer par des use case, qui ciblent les utilisateurs types (persona)

Ce qui va changer, c'est le vocabulaire utilisé, ou décomposer la demande différemment selon le niveau. 

Mais aussi pour permettre à l'utilisateur de monter en compétence et gagner en vocabulaire.

persona: "souhaitez-vous être accompagné" ou "recherche simple" vs "avancé". Ne pas cliver débutant/avancé.

mettre 2 niveaux, les utilisateurs choisiront leur niveau.

Dans les deux cas il faut un formulaire simple et détaillé.

Jouer avec des "mise en lumière" de parties du site.

Voir aussi walkme et l'aspect démo.





**Deux alternatives possibles pour le portail:**

A1: Entrée soit par "accès débutant" soit par "accès avancé" (ne pas garder cette terminologie ensuite) pour dérouler ensuite un questionnaire adapté à l'utilisateur. Note: sur la  V1 que 2 niveaux, pour Versions suivantes rajouter les aspects tuto et MOOC.

Aspect positifs: permet de monter en compétence si on détaille les étapes et les définitions **(à compléter : devoirs pour le 10/06)**

   * une interface déjà quasi bouclée grâce au POC pour les utilisateurs experts
   * des objectifs clairs pour chaque interface, gain en lisibilité pour les 2 types d'utilisateurs
   * plus satisfaisant du point de vue développement car: 2 tâches distinctes, possible 2 techno, 2 épisodes différents...




Aspects négatifs:

   * 2 interfaces indépendantes donc double boulot ?
   * demande à l'utilisateur de se définir, alors qu'il ne sait peut-être pas le faire?


Exemples: [https://ngphylogeny.fr/](https://ngphylogeny.fr/);...



A2: Proposer le formulaire de choix des critères (dans sa globalité) à tous les utilisateurs, mais avec des aides à plusieurs niveaux (avec des zones en surbrillance) pour accompagner les néophytes.

Aspects positifs:  **(à compléter : devoirs pour le 10/06)**

   * une seule interface donc moins de boulot ? quoi que...
   * montée en compétence du débutant facilitée puisqu'il s'agit de la même interface (le débutant n'aura plus qu'à ne plus demander l'assistance en ligne)
   * à compléter svp ;)
   * Boulot de conception progressif: on peut mettre en prod et peu à peu rajouter de l'aide pour débutants
   * 



Aspects négatifs : 

   * peur de noyer le débutant; surcharge cognitive.
   * contraint à suivre la trame de l'interface "expert" et y prévoir les aides ad hoc sans trop surcharger pour les débutants
   * risque du "ni fait ni à faire" et on ne répond pas au débutant tout en barbant l'expert (le pire)
   * 



Exemples : 

    

**A faire**: Se positionner sur le choix de la meilleure alternative entre A1 et A2 selon nous **(devoirs pour le 10/06)** :

    - Emilie : à la réflexion je me dis que l'alternative A2 semble plus efficace et réaliste, au moins dans un premier temps : on partirait sur le développement de l'interface type "expert" sur le modèle du POC à laquelle on y ajouterait une "surcouche accompagnante" que l'utilisateur débutant pourrait activer (désolée pour mes termes pas techniques). Cela permet d'avoir un résultat quoi qu'il arrive, avec l'assistance qui va évoluer (versions suivantes) aussi en fonction des retours des utilisateurs (si on garde l'idée de questionner les utilisateurs au moins durant la première année de mise en prod).

    - Eric: On peut finalement se laisser le temps du choix? En commençant par A2, et en développant l'interface expert, puis en rajoutant la "surcouche accompagnante" si on voit que les tecno diffèrent trop, ou que cela devient trop "surchargé" on peut séparer les deux interfaces et retomber sur A1, sinon on poursuit sur A2.

    

    

    

# Visio du 10/06



vidéo bonnes pratiques UX : [https://www.youtube.com/watch?v=WsHNxOcOJTw\&t=2177s](https://www.youtube.com/watch?v=WsHNxOcOJTw\&t=2177s)

site web avec bonnes pratiques (Do / Don't) : [https://uxdesign.cc/form-best-practices-8e560e9f8bd0](https://uxdesign.cc/form-best-practices-8e560e9f8bd0)

et pour le filtrage : [https://uxplanet.org/9-filtering-design-best-practices-to-improve-e-commerce-ux-edac50560f94](https://uxplanet.org/9-filtering-design-best-practices-to-improve-e-commerce-ux-edac50560f94)



Le type de formulaire (1 ou 2 colonnes, suivi par étape séquentielle, etc.) va être impacté par le nombre de critères.

Faut-il une recherche simple et une avancée? Cela va aussi déterminé l'affichage.

Travailler le regroupement des critères par logique. Le POC actuel mélange un peu tout.

Premier niveau pour orienter soit vers le calcul soit vers le stockage et éventuellement vers le cloud, avant le niveau expert/débutant.



Important de montrer le tableau de résultat filtré ou au moins le nombre de résultats filtrés à chaque sélection de critères ?

Est-ce qu'on veut faire un filtre ou une recherche ? Le POC actuel fait un filtre (inspiré de [https://www.hetzner.com/dedicated-rootserver)](https://www.hetzner.com/dedicated-rootserver)). Comme leboncoin ou booking.

Peut-être une combinaison recherche/filtre?

Peut-être proposer un comparateur? Se prête plus à du choix de stockage?

Le filtre est rassurant car il donne des résultats même si pas beaucoup de possibilités offertes au global (100 calcul, 20 stockage, moins de 20 cloud)

Quid données sensibles ? --> faire remonter au groupe critères la question sécurisation des données



Premier niveau (avec des couleurs?); 

Proposer des filtres standards, et des filtres "avancé" "+ de filtre"



Devoirs : dessiner ! 

Oana, Eric, Nadia, Cyril: ébauchent une interface et on en discute ensemble.

Outils possibles : [http://wireflow.co/](http://wireflow.co/); draw.io

Cela nous permettra de choisir un wireframe; Daniel proposera une maquette par la suite.



Clarification des rôles entre groupes doc et UX : Ux travaille sur la forme et propose un cheminement pour que documentation (qui travaille sur le fond) organise la doc.

Pour l'instant une interface différente. 





# Visio du 15/06



Discussion de la proposition de l'axe OAD d'implanter une carte interactive des localisations des ressources affichées: où l'intègrer?

Voir la proposition de Daniel du site des infra de l'INSERM: [https://infrastructures.inserm.fr/Pages/Recherche.aspx#ListResults](https://infrastructures.inserm.fr/Pages/Recherche.aspx#ListResults)



Une possibilité est, une page d'accueil de contexte et de choix (calcul, stockage, cloud) puis on affiche style INSERM, carte droite et critères à gauche, mais pas sûr que ce soit les choix premiers des utilisateurs

Il faut expliquer à l'utilisateur en amont en leur proposant des scénarios (marche à suivre) en 3 étapes simples (ex calcul, consiste à)

       * rech l'espace de calcul
       * trouver espace stockage pour ce calcul
       * rech le stockage de plus long termes une fois calcul effectué


Ajouter à cela des définitions pour ceux qui le désire (bouton "?") avec arborescence ou schéma. A voir avec le groupe Doc pour se coordonner.



**Concrètement pour notre interface:**

       * une 1ere page avec des scénarios = macro étapes: 1, choix localisation, 2, choix techno, 3, résultats
       * avec pour aller plus loin, de l'info venant de la doc (qui se superpose?)
       * si l'utilisateur fait un choix de scénario, il va vers l'interface avec des puch informationnel (Interface recherche avec nav gch, carte qui peut se masquer.)


Voir par ex la page d'accueil INSERM avec explication des pictogrammes: [https://infrastructures.inserm.fr/Pages/Accueil.aspx](https://infrastructures.inserm.fr/Pages/Accueil.aspx)



 Scénarii et détails (cf. doc). 

 

** Pour mercredi 17/06 9h30 (devoirs)** 

- On fait des propositions de page d'accueil sur Mattermost. pour mercredi (sur la base des notes ci-dessus)

- Ou au moins trouver des sites inspirants qui montrent ce que l'on souhaite

- S'il a le temps, JF Rey intègre la carte dans le site actuel et modifie le template d'Hugo pour mettre les critères à gauche et résultats en dessous. 


