
# Grille auto-organisation Hackathon PCSC

# Axe Documentation



### Nom du groupe : 

## 



## Membres inscrits

*Lister les membres et se présenter*

   * Cyril Jousse (Univ Clermont Auvergne, interface biologie-chimie, métabolomique)
       * "La doc doit être compréhensible par un boulet ... comme moi !!"
       * Lien UX
   * Lise Frappier (INRAE, Rennes, DipSO + SMART-LERECO - économistes) Ingéniérie pédagogique (e-formations, capsules vidéo) et utilisateur super ignorante
       * Lien orga
   * Estelle Ancelet (INRAE Toulouse, unité MIAT, équipe Record) Développeuse, administratrice instance Galaxy, utilisatrice du mesocentre meso@LR (commandes de base slurm)
       * relire de la doc
       * Lien critères
   * Emmanuel Braux (IMT Atlantique) Ingé système et réseau, administrateur infra cloud privé (openstack)
       * expérience portail doc, pas bcp de temps
   * Emilie Lerigoleur (CNRS, UMR GEODE Toulouse) géomaticienne
       * béotienne, coordination projet ANR SO, mise en place e-infra (accompagnement choix centre calcul), tester la doc
       * Lien UX
   * Tovo
       * Coordinateur pcsc, Observateur de ce groupe


## Rôles

Un rôle correspond à une “casquette”, chacun peut changer de casquette ou en porter plusieurs.

Vous pouvez changer les noms des rôles - A vos bonnes idées....

   * ***Huggy les bon tuyaux**** : chargé(s) de faire le lien avec l’équipe d’organisation et les autres groupes - Lise*
   * *Rôles spécifiques aux réunions  (rôles tournant)*
       * ***scribes**** : chargé de prendre les notes pendant les réunions*
       * ***cadenceurs**** : chargé de gérer le temps pendant les réunions*
   * ***La Balance**** : présente les résultats du groupe en plénière —> penser à le nommer quand un retour du groupe en plénière est annoncé*
       * 5/06 - Cyril
       * 12/06 - Lise
   * ***Le Boulet**** : rappelle que ça doit être adapté à tous les types d’utilisateurs*
       * tous
   * ***Dirty Harry**** : s’assure que tout avance, que les membres ne sont pas perdus - Lise*
## 

## 

# Comment rédiger (techniquement) la documentation

## Concept

Les fichiers de la partie documentation sont situés dans le répertoire content/



# Actions



   * Jeudi 11/6
       * Tovo crée le sondage dans Mattermost pour les réunions semaine 25
       * Estelle remet en ordre le pad
       * Lise fait la restitution à la plénière du vendredi 12/6
           * Elle demande à cette plénière si doc à destination des rédacteurs doit également être centralisée dans le portail pcsc ou ailleurs pour ne pas perdre l'utilisateur lambda
   * Mardi 9/6
       * Émilie voit le 10/6 matin avec UX comment se répartir les rôles entre les 2 groupes. A voir ensuite comment on s'organise.


## TODO \& liens avec autres équipes



Lister les questions auxquelles la doc répondra --> recueillir auprès des participants les questions qu'ils se posent - Lié avec use cases du groupe UX

Qui aura quel rôle dans la rédaction de la doc ? **à voir avec gouvernance**. La question se pose notamment si on considère qu'un responsable de plateforme ne peut pas rajouter par lui même un critère (par exemple) et qu'il faut une validation du groupe pcsc.

Indiquer à UX qu'il faudrait 2 niveaux pour la définition des termes (débutant et plus avancé).

Définir en lien avec l'axe UX, le/les parcour(s) utilisateurs.

Lien avec chatbot ? avec ontologies pour le vocabulaire adéquat ? avec UX pour user cases



## Cadrage



### Objectifs du groupe

Produire l'architecture de la documentation et le processus de production de documentation

cf. idées formulées lors du brainstorming du jeudi 28 mai matin : [https://etherpad.in2p3.fr/p/pcsc\_brainstorming\_202005280900](https://etherpad.in2p3.fr/p/pcsc\_brainstorming\_202005280900)





   * Lundi 15/06 
       * 13h - réflexion sur l'arborescence (récupérer une liste des critères ?) et point sur les liens avec les autres groupes
       * 16h30 - Présentation de la rédaction de la doc sur forgemia en vue d'une publication sur le portail
   * Jeudi 11/6
       * Formalisation des objectifs sous forme de mindmap
   * Mardi 9/6
       * Définition des objectifs
   * Jeudi 4/6
       * Tour de table


### Public cible

3 rôles sont définis en fonction des profils utilisateurs envisageables.

   * Utilisateurs lecteurs : utilisateurs du portail pcsc
       * 2 parcours envisagés 
           * Débutants (certains n'ont pas envie de monter en compétences / d'autres si...)
           * Experts/Avancé
   * Scénaristes :  créent la structure : quel type de contenu, où, nombre de caractères, .... 
   * Utilisateurs rédacteurs : rédigent les blocs
   * Penser à la version anglaise de la documentation --> Google trad + relecture anglophones


### **Ce que l'on va **documenter :

    

En se basant sur une carte mentale :

[https://drive.google.com/file/d/1ROUwDrwHvQMiVMALBcQYGZ6PD8fY1wUZ/view?usp=sharing](https://drive.google.com/file/d/1ROUwDrwHvQMiVMALBcQYGZ6PD8fY1wUZ/view?usp=sharing)

Avec extraits réguliers sur Mattermost



#### Pour les utilisateurs lecteurs

   * critères - documenter le vocabulaire utilisé (donner la possibilité d'approfondir, avec différents niveaux d'approfondissement) - glossaire 
       * à définir avec les axes critères qui travaille beaucoup orienté OAD
       * lien ontologies    
       * lien avec UX : il faudra des niveaux pour les définitions
   * prise en main pcsc
       * Documenter la navigation dans le site --> préparer une trame grâce à critères et UX ? on aura le détail
       * Définir en lien avec l'axe UX, le/les parcour(s) utilisateurs
   * méthodologie
       * Voir si la doc doit être modulable sous format de bloc unitaire simple et court, pour être consultée morceau par morceau ou assemblée pour suivre un parcours
       * Développer des parcours de montée en compétences sur les pratiques --> fiches pratiques ?
           * lien vers tag des plateformes MOOC
       * Page d'accueil - lien avec UX
           * scénarios choix utilisateur sur les infos dont il a besoin


#### Pour les utilisateurs rédacteurs

   * les rédacteurs (souvent les pigistes) rédigent les blocs
   * But : savoir comment alimenter pcsc, comment structurer la documentation ?, est-ce que ça doit être visuel ? Quelles informations y placer ?
   * Comment voir son infra sur pcsc
   * Expliquer comment rédiger la doc
   * Savoir mettre à jour les infos : pour que les rédacteurs resp plateforme, sachent comment décrire leurs plateformes
   * Définir comment la doc sera produite (quelles interfaces ?) Comment faire en sorte que des personnes participent à la rédaction de docs ?
       * quel langage / format : Hugo -> markdown ?
       * quel outil : écriture texte brut, outil WYSIWYG, ...
       * **[]Tovo a un début de réponse ??[]**
#### 



#### Pour les utilisateurs scénaristes 

   * Les éditeur (scénaristes) créent la structure : quel type de contenu, où, nombre de caractères, .... 
   * Fait-on une doc pour les scénaristes ? Pas sûr. Faut-il laisser la case ? À minima il y aura un doc sur comment nous avons structuré la documentation.




### **Ce qu'on ne fera pas** :

   *     Une doc sur l'utilisation des plateformes






## Contenu des documentations



### Général

Testeurs : béotiens et informaticiens

La doc pour l'utilisateur lecteur apparaîtra dans l'existant. Pour le reste on demande demain en plénière.



La doc doit être au moins en français et anglais pour diffusion du portail à l'étranger + familiarisation des utilisateurs avec les termes anglais. 





### Utilisateurs lecteurs

Comment assurer l'auto-formation des utilisateurs sur la plateforme pour une prise en main facile ?

La concevoir comme un MOOC ? 

Comment construire l'architecture du parcours ? 

   * carte mentale ? Autre chose ? 
   * Besoin : voir les entrées, et l'évolution dans les parcours, identifier les contenus identiques 
Quelles informations y placer ?

   * tutos - html / vidéos ?
       * utilisation de PCSC
       * règles d'accès aux plateformes référencées
       * intérêt d'un cluster de calcul ou d'infra distantes / local
       * comment gérer ses données, son stockage, gérer des gros jeux de données, outils adaptés
   * questionnaire guide : vous avez besoin de... allez là ou c'est le chatbot qui fait ça ?
   * créer une liste de formations curées (à présenter sous forme de parcours de compétences ?) et une liste de documentations existantes ailleurs
       * Négocier d'héberger les ressources essentielles pour s'assurer de leur disponibilités 
   * Des exemples/cas d'usage : "comment j'ai fait pour lancer mes simulations xxx sur le cloud YYY"
Quel parcours utilisateur ?

   * 2 stratégies
       * Dans la lignée de ce qui a été dit dans le groupe UX, choix de 2 ou 3 interfaces distinctes suivant le niveau (ex: [https://ngphylogeny.fr/)](https://ngphylogeny.fr/))
       * 1 seule interface, avec une proposition d'aide (aide à scénariser) --> à privilégier ?
           * documentation 3 niveaux simple / avancé / expert


Pour rentrer dans du concret.

Suite à la réunion du 15/6 de l'UX, il faudrait faire :

   *     Page d'accueil sur le modèle [https://infrastructures.inserm.fr/Pages/Accueil.aspx](https://infrastructures.inserm.fr/Pages/Accueil.aspx) avec
       * Avec 3 carrés pour calcul, stockage, cloud
       * + 1 carré "Rechercher directement" : [https://infrastructures.inserm.fr/Pages/Recherche.aspx](https://infrastructures.inserm.fr/Pages/Recherche.aspx)
       * Des scénarios
       * Un cheminement qui oriente le lecteur


On rédige 1 paragraphe d'intro sur le but du portail.

Et 3 petites définitions pour calcul, stockage et cloud. Ces définitions étant plutôt indépendantes.

Il faut faire comprendre à l'utilisateur pourquoi il y a 3 entrées même s'ils veulent les 3.

Lise peut proposer des définitions issues de plusieurs dico. Tovo peut relire les paragraphes. Mais ne devrait pas demander à l'axe "Ontologies" ?

Attention à que ce soit compréhensible par tout le monde. 

Des visuels qui accompagnent chaque bloc, ce serait bien.

   * Calcul
       * Image / Picto en un coup d'oeil
       * Définition de base : 
       * Qu'est-ce que le calcul et quand est-ce qu'on a besoin d'une infrastructure ? - Détaillé
       * Stockage possible mais souvent pas pérenne et pas sauvegardé
       * Souvent pas d'interface
   * Stockage
       * Différents types de stockage (scratch, court/moyen/long terme)
       * Quand est-ce qu'on a besoin d'une infrastructure de stockage
   * Cloud
       * Qu'est-ce que le cloud ? (différents type de cloud [calcul/stockage/SaaS]) accès dématérialisé
       * Spécificité du cloud par rapport aux infrastructures traditionnelles ( ressources mobilisables à la demande, ubiquitaire, configurable)
       * Peut faire du calcul et du stockage




### Utilisateurs rédacteurs

Travail de scénarisation à faire.  à rendre transparent pour les rédacteurs ? ou les accompagner dans la scénarisation ?

Scénariser

   * Objectif pédagogique général : A la fin de la séquence l'utilisateur saura...verbe action
   * Déroulé : Découverte / apports / évaluation de l'apprentissage
Où est-ce que l'utilisateur trouve la Doc ?

Pas dès la page d'accueil, avoir une entrée admins ? 







## La forme des documentations



**Possibilités techniques et limitations avec Hugo**

Comment peut-on gérer l' "assemblage" de contenu ?

Quelles possibilités au niveau dynamique, recherche ?

Quelles possibilités au niveau outils d'assistance à la rédaction ?









   * Moyens nécessaires (temps, outils)
   * Risques
   * Évaluation des risques




### Quels sont les Risques ===> les Solutions possibles correspondantes :

*risque x ===> solution de x *   

oublier des publics --> faire relire aux autres groupes / faire tester

    

 

 

   * Plan d’action / tâches / affecté à qui ? —> évolutif dans le temps, on se détend...
Définitions de portail / calcul / cloud





Recenser les formations en ligne qui existent déjà et les catégoriser - identifier ce qui manque et synthétiser des connaissances


