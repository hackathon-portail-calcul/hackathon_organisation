# PCSC Brainstorm
27 mai 2020 - 13h\_19h

## Présents :
    - Jacques Lagnel
    - Jérémy Verrier (remplir les données sur les centres, rédaction de doc accompagnante pour le chercheur: premiers pas) 
    - Estelle Ancelet (faire un retour d'expérience sur leur recherche de solutions, test grandeur nature sur cas concret, définitions des critères, relire de la doc, gitlab + CI, dev HUGO)
    - Patrick Chabrier (définitions critères, vérification que une personne de niveau intermédiaire comprenne la doc et interface)
    - Pierre Gay (format de fichier)
    - Richard Randriatoamanana
    - Cyril Jousse (apporter des uses cases, retour d'expérience sur ce qui se fait dans metabo-hub = conception de cahier des charge, 
                            benchmarking d'outils, certifications, faire des demandes pour avoir des RH ou $)
 

## Warning et remarques:
    - ok pour un portail mais faut-il que les end-users trouvent le portail : ne pas négliger la com
    - accompagner : cours
    - le kick-off ne remettais pas assez le contexte et la problématique, trop technique, trop fonctionnel 
    - comment réussir à contribuer ? quelle est l'orga
    - suis-je assez compétent pour aider
    - kick-off trop rapide sur certains aspects : 
        - périmètre inter-instituts, France ? Europe ? Entreprises ?
        - périmètre : que calcul ? juste du matériel ? des services ?
        - architecture de l'outil : trop rapide
        - est-ce un annuaire qui moissonne d'autres annuaires ? une base de donnée ?
    - projet a une réelle utilité, même une nécessité
    - orga pas assez claire

    - orga souple mais quelle est la couverture des participants sur les instituts ? Sont-il convaincus ?    
    - quelle soutien institutionnel, quelle pérennité ? (très important) avoir de bonne fondations
    - kick-off : format court apprécié
    - kick-off : bien, plein de user d'horizons différents
    - kick-off : bien pour donner le top-départ
    - kick-off : court si beaucoup de personne
    - brainstorming bien pour trouver les idées de chacun
    - comment construire le moteur ? 
    - s'inspirer des success stories : 
        - zenodo (moissonnage d'infos bien fait, cf invenio) 

   *   des outils comme dataverse ou invenio ne seraient-ils pas pertinents ?    
   - risque vis-à-vis des infras qui ne veulent pas être comparer à d'autres 

        -> éviter le côté commercial, les infras ne sont pas des objets de consommation courante, éviter les notations)
   
   - qui sera responsable d'information ?

   *     - auteur de la fiche
   * - orga et techno plait beaucoup
   * - outil chouette car l'outil permet à l'utilisateur de fouiller le ressources et même de découvrir des solutions et besoins dont il aurait besoin
   * - OAD ++
   * - importance pour les infras : pas une vitrine mais un endroit où trouver les ressources
   * - attention à être capable de proposer une interface simple pour les débutants
   * - il faut que les critères soient en phase avec les besoins des utilisateurs (jusqu'où aller dans le détail)
   * - bien formaliser les conditions d'accès aux ressources dans les critères
   * - montrer aux utilisateurs le taux de charge en calcul des infras n'a pas de sens pour le calcul, ni pour le stockage
   * - il ne faut pas que le portail perde les users
   * - bien de mélanger les mondes des infras
   * - il faut commencer petit 
   * - se faire certifier pour convaincre, apporter des garanties 

## Recueil des idées (sujet, partie, domaine)

- autre exemple donné par Esther D. : \url{https://fairsharing.org/}
- avoir une boite à idées centralisée (channel mattermost + fichier md dans le git)

- utilisation des platforms (la base de l'utilisation) : cours sexy

   * - worshop
   * - webinaire
   * - video explication
   * - tutos pas à pas- besoin de fiabilité sur les outils : container, booster  \url{https://groupes.renater.fr/sympa/info/csan}
- connaitre le outils minimaux sur les infras (SLURM, Singularity) 
- connaitre la charge des infras et savoir si le besoin sera réellement accueilli et non juste en théorie
- mettre en place un kaban board?
- boite à idée pour l'OAD
- des outils comme dataverse ou invenio ne seraient-ils pas pertinents ?
- penser à être moissonnable et pouvoir moissonner d'autres sites (cf métadonnées )
- mettre un responsable de fiche, qui rempli la fiche et qu'on sache qui il est (identification !!)

   * -> qualité, pas d'usurpation d'identité
   * -> celui qui remplit fait parti de l'infra
   * 2 choix possibles pour remplir/mettre à jour les fiches : 
       * - petit groupes qui remplit
       * - chacun maintient sa fiche (valeur ajouté)- maturité des infra :

   * - indiquer si une infra est aussi une infra européenne, infra d'un meta-projet, d'une e-infra de xxxx, ...- lier le glossaire à l'oad

   * - donner la définition des termes en dynamique depuis l'OAD- format stable
- un endroit unique pour mettre les descriptions des ressources
- les quotas sont dynamiques, doivent-ils être stockés dans des fichiers à part
- se servir de PCSC pour réfléchir sur l'orga des meso/infras et pour soumettre des demandes de ressources

   * - créer une fédération 
   * - créer une authentification unique entre les infras (mobilité des users)- partir de uses cases et de users stories pour adapter et être sûr que l'outil 


###  Critères : 
    - données sous licence
    - softs sous licences
    - aspects organisationnel : condition d'accès

   *     - appel d'offre ou au fil de l'eau         - temps de latence pour avoir accès aux ressources ?
    - unix linux
    - quel type de scheduler ?
    - containeurisation possible ?
    - permettre l'adéquation avec le niveau de compétences de l'utilisateur (en lien avec les outils et services)
    - prix (et couts cachés)
    - pouvoir héberger un site web qui accède aux ressources
    - protocoles d'accès 
    - types montages possible
    - critères de mobilité:

   *         possibilité de classer les centres de ressources par ceux qui se ressemble
   * - possibilité de comparer les ressources sélectionnées
   * - accompagnement, doc, formation proposée comme un service
   * - VO de la grille acceptée par l'infra
   * - comment se place PCSC par rapport aux feuilles de route des e-infra (elixir, IFB, ...) ?
   * - HDS
   * - PGD (acceptation du users si PGD)
   * - certifications (RDA, HDS, )
   * - PGDready
   * - coût écologique (C02, e-, ...) des infras (efficience des salles/clim, types de stockage, ...) +
   * - quotas: 
       * - par user
       * - par groupe


   * Description des infras :
       * - comptes twitter, Facebook       
    
