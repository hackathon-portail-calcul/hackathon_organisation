# PCSC Brainstorm
27 mai 2020 - 9h\_13h

## Présents :
- Olivier Langella (Dev JS/UI)
- Mikael Loaec
- Gilles Mathieu (Réflexions, use-cases, besoins)
- Raphaël Flores (CI, processus d'alimentation en données par les plateformes, ...)
- Sophie Aubin (Définitions/Vocabulaire pour la partie Documentation et schéma de la plateforme)
- Jacques Lagnel

## Recueil des idées (sujet, partie, domaine)
- Quelle forge pour la version finale ?
- Comment curer les données json ? (manuel ou automatisé avec un plugin installé dans les plateformes)
- Viser trop haut, trop exhaustif et difficile à maintenir ... objectif du V1 ?
- Doc : Catalogue de formation d'usage des différents types de ressource, permettant de partager les bonnes pratiques
- OAD : Critères de recherche en fonction des besoins => ie. guider avec des questions générales puis de plus en plus ciblées pour identifier les ressources à utiliser
- Quelle gouvernance ? Qui sont les acteurs et quels rôles auront-ils ?  plutôt orienté utilisateurs (on s'assure de répondre aux besoins)  / plutôt orienté fournisseurs (on s'assure de la mise à jour des données MAIS risque que le portail devienne une vitrine commerciale) . Il faut trouver un équilibre
- Pour faciliter la mise à jour des données par les infrastructures, le portail doit se positionner comme fournisseur d'informations pour des sites plus spécifiques qui référencent ces mêmes services/ressources. Les données d'une e-fra ou d'une organisation par exemple doivent pouvoir être intégrées facilement dans un site web au moyen d'un widget, iframe, contenu json, etc.  
- Quid des offres commerciales avec lesquelles des instituts de l'ESR auraient des accords ? Pourraient-ils être visibles parmi les ressources de l'outil ?
- Quelles interactions avec les autres inventaires (IFB, France Grille...) ? Le portail permet d'industrialiser le recensement des ressources informatiques disponibles.
- Quels sont les outils disponibles/utilisables sur une plateforme ? (SLURM, Singularity, ...)
- forge CI-CD pour les images singularity (pull via oras)
- Est-il possible de référencer (en temps réel ?) le niveau de charge des différentes ressources disponibles (stockage disponible, RAM \& CPU utilisée ??), pour guider des utilisateurs vers d'autres plateforme similaire mais disposant de plus de ressources disponibles réellement. Il faut aussi prendre en compte la distance donnée/calcul pour faire des recommandations pertinentes. Un des objectifs étant de limiter le développement en taille de plateformes très populaires quand d'autres sont dispo (développement durable) Objectif pas forcément aligné avec ceux de l'état qui préconise l'utilisation de quelques gros data centers.
- Documentation/Définitions; Pour les utilisateurs de l'outil (ils ne maîtrisent pas forcément toute la terminologie ou peuvent avoir des doutes). Pour les fournisseurs, ces définitions pourraient être incluses dans le schéma de la plateforme pour s'assurer de la cohérence.
- Existe-t-il des vocabulaires mentionnant les concepts manipulés (CPU, RAM, stockage...) : regarder dans LOV (\url{https://lov.linkeddata.es/dataset/lov)}, ie. \url{http://cookingbigdata.com/linkeddata/ccinstances/}
- dans un premier temps, le portail est en français. On peut viser un portail bilingue, notamment pour des questions de visibilité (dont au niveau européen), mais aussi pour nos utilisateurs non francophones. Les définitions pourront présenter les termes en français et en anglais.
- OAD : les menus de filtres (ex: localisation) ne devraient contenir que des valeurs pour lesquelles il existe des résultats. Solution (?) : alimenter les listes avec les valeurs présentes dans le json. 
- Comment les formulaires sont alimentés?
- Sur la gestion des dépendances de la CI Gitlab, envisager de les mettre en cache d'un job à l'autre, cf. \url{https://docs.gitlab.com/ce/ci/caching/index.html}

