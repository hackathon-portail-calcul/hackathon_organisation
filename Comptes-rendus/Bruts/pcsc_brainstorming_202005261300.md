# PCSC Brainstorm
26 mai 2020 - 13h_19h

## Présents :
- Cédric Goby (IP INRAE, data, informaticien)
- David Benaben (PF CBIB/IFP INRAE, adminsys)


## Warnings/remarques :
- attention on part à 50, on finie à 3 et la pérennité en prend un coup
- attention l'obsolescence des données !!!! -
- difficulté à ce projeter dans le hackathon : bcp de monde, nouveaux format, temps limité
- ne pas chercher l'exhaustivité des paramètres
- qui a le droit de modifier les données des infras
- une infra a plusieurs "infra" en interne (effet poupée russe des infras)
- difficulté de faire rentrer l'infra dans les case de critères (cf champs libre)
- Peut-être ajouter quelque chose autour de la politique d'accès ? Par exemple, l'Infrastructure Française de Bioinformatique est un institut national (ie pas forcément visible si je cherche quelque chose dans ma région) mais ouvert à toute la communauté des bioinformaticiens/biologistes.
- comment lister les portails galaxy, rstudio, rshiny 

## Recueil des idées (sujet, partie, domaine)

- pointer des personnes contacts dans l'infra mais aussi généralistes (centre, institut, unité, ...) en fonction du domaine (cluster, data, cloud)
- salon de discussion : pour de l'aide ponctuelle/interactive 
   => exemple tchap : salon de discussion avec grosse plus value  interministériel 

         - pas de maintenance
         - facile à mettre en place
         - outil souverain et sécurisé
         - inscription institutionnelle via mail pro mais 
              - pas d'inscription autonome pour hors des ministères (mais invitation possible)
              - besoin d'une clef
             - si perte de clef alors perte des discussions
         - gestion des clefs de chiffrement peut-être compliqué
         - pas de sous-salon possible (pour l'instant, à l'avenir ?)
 - inclure des recommandations d'infras en fonction de l'origine de l'utilisateur (ex tel institut/université/labo recommande tel et tel infra = prioriser la présentation ou mettre des tag de recommandation
 - inclure des warnings de recommandation en fonction de l'origine des utilisateur et de l'infra (pb de sécurité, cloud act)
 - attention à la doc qui va avec
 - API pour mettre à jour 

      - surtout très utile pour ce qui est changeant (logiciels, lib, ...)
 - API s'oppose/complémentaire à la génération du json complet par le fournisseur d'infra
 - oad : 

      - une version simple pour les débutants + recherche avancée
      - case à cocher disant si on est débutant => besoin d'accompagnement humain ou solution "facile"
-   attention l'obsolescence des données

     - les fournisseurs remplissent leurs données (décentraliser le remplissage)
     - péremption des données (disparition des données si pas mises à jour après x temps) : il faut mieux ne pas avoir de données que d'avoir des données obsolètes
     - relancer les fournisseurs avant échéances
- avoir un contact pour PCSC facile à trouver : tel et/ou mail et/ou salon de discussion

     - pb technique avec PCSC
     - pb sur les valeurs des données
     - pb d'utilisation de PCSC
     - attention sur les détails des paramètres (GPU Xa123, GPU Xv23, ...) et préférer un gros champs libre avec recherche à la google
- quelques remarques sur les "critères": https://forgemia.inra.fr/hackathon-portail-calcul/existant/-/issues/1

     * coreNumber: je préciserais si cela s’entend avec "HyperThread" ou non (moi je suis pour compter les cœurs sans hyperthread :p )
     * Storage: Je trouve le champ assez ambigu. Par exemple, est-ce qu'il faut aussi ajouter l'espace utilisé pour la réplication ? Est-ce que je dois noter tous les espaces de stockage de la plateforme même s'il ne sont pas directement lié au cluster (iRODS, sauvegarde) ? Est-ce qu'on doit, en plus de l'espace partagé, comptabiliser les disques locaux des serveurs de calcul ?
     * Storage: Le chiffre donne une bonne idée de la "taille de l'infrastructure" mais n'est pas forcément pertinente pour l'utilisateur (tout les espaces sont-il utilisables ?, Quota ?, etc.)
     * Contact: Je sus mitigé entre mettre un contact technique ou le responsable de la structure. mettre les 2
     * networkBandwith = LAN ou WAN ? Type (Ethernet / Infiniband) ?
     * Ajouter des tags pour décrire en quelques mots clefs l'infrastructure ("mésocentre", "bioinformatique", etc.) ?
- OAD : critères à prendre en compte

     - prix
     - protocole d'accès
     - logiciels d'accès (winscp, rclone, filezilla, webdrive=outils-payant-photos, )
        ◦ créer des fichiers et structures de données de correspondance entre les softs et les protocoles (et autres fonctionnalités), attention au version
     - taille des fichiers et nombre de fichiers
     - type de fichier (photos, ...)
     - capacité à offrir des fonctionnalités de diffusion (URI, URL, ...)
     - sécurisation des données
         - réplication
         - versionning
         - sauvegarde
         - snapshot
         - ...
     - bande passante
     - performance
     - quota
     - version de MPI 
     - quels logiciels et quelles versions
     
Critères en cascade (arbre) sur les critères

    * exemple GPU > génération de GPU > version de modèle > RAM des modèles > nombre de GPU par serveur
Avec des champs libre à tous les étages de la cascade !!!

AOD : mettre une description standardisée et sympa dans les choix + un page complète et sympa pour chaque

### Portail XSEDE:

https://www.xsede.org/

https://portal.xsede.org/

### Portail EOSC :
    https://marketplace.eosc-portal.eu
    
https://cat.opidor.fr/index.php/Cat_OPIDoR,_wiki_des_services_d%C3%A9di%C3%A9s_aux_donn%C3%A9es_de_la_recherche


## Intérêt pour les infras :
    - visibilité
    - soulagement sur l'accompagnement 

     - plus efficace car un humain a une vision plus parcellaire des ressources
     - pourrait éviter au meta-infra de faire le travail de recensement et diffusion (travail en cours à l'IFB)
         * IFB : fichier excel pénible à remplir 
         * demande annuelle faite par mail pour mise à jour
         * inclus des métriques d'utilisation
     - idem pour le cloud mais plus automatique
