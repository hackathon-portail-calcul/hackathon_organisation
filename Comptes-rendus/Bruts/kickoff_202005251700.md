# PCSC kick-off 
17h, le 25 mai 2020

* 9h-10h https://etherpad.in2p3.fr/p/pcsc\_kickoff\_202005250900
* 11h-12h https://etherpad.in2p3.fr/p/pcsc\_kickoff\_202005251100
* 14h-15h https://etherpad.in2p3.fr/p/pcsc\_kickoff\_202005251400
* 17h-18h https://etherpad.in2p3.fr/p/pcsc\_kickoff\_202005251700

## Présents :
- Tovo Rabemanantsoa, INRAE, tovo.rabemanantsoa@inrae.fr
-  Alexandre Dehne Garcia, INRAE, alexandre.dehne-garcia@inrae.fr
-  Gilles Mathieu, INSERM, gilles.mathieu@inserm.fr
- Fred de Lamotte, INRAe, frederic.de-lamotte@inrae.fr
- Arnaud Jean-Charles, CNRS, jeancharles@eccorev.fr 
- Sumaira JAVAID, javaid.sumaira@gmail.com
- Emmanuelle Morin, INRAe, emmanuelle.morin@inrae.fr
- Paul Rinaudo, Polytechnique, paul.rinaudo@polytechnique.edu
- Anne Laurent, Université de Montpellier, anne.laurent@umontpellier.fr
- Sébastien Cat, DSI INRAE, sebastien.cat@inrae.fr
- Mikael Loaec, INRAE, mikael.loaec@inrae.fr

## Questions/réponses :
* Comment on se positionne par rapport à l'IFB ?
* Est-ce qu'on envisage que les données soient interrogeables par autre chose que le portail (API, SPARQL...) ?
* Mise à jour des informations ?
* Comment garantir que les données soient à jour et pertinentes (+qualité, +validation) ?
* Au vu des volumes et typologies, comment gérer l'ensemble des données ?
* Notion de labellisation des infrastructures ?
    
## Idées :
* Lien données/calcul
* Conservation des données output dans les critères
* Dynamisme, interaction dans le portail

## Remarques et Points de vigilance :
* Modalités d'accès
    
