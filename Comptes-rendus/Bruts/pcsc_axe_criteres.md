
# Hackathon PCSC - Axe Critères



Ressources à consulter pour inspiration:

   * [https://www.eoscpilot.eu/](https://www.eoscpilot.eu/) / [https://www.eosc-portal.eu/](https://www.eosc-portal.eu/)
   * Ancienne version PCSC: [https://forgemia.inra.fr/ingenum/pcs/-/tree/master/data](https://forgemia.inra.fr/ingenum/pcs/-/tree/master/data)
   * Infos demandés par l'IFB: [https://forgemia.inra.fr/hackathon-portail-calcul/hackathon\_organisation/-/tree/master/Crit%C3%A8res/ifb-indicateurs](https://forgemia.inra.fr/hackathon-portail-calcul/hackathon\_organisation/-/tree/master/Crit%C3%A8res/ifb-indicateurs)
   * standards glue et glue2 (grille) : [http://gridinfo.web.cern.ch/glue](http://gridinfo.web.cern.ch/glue) ; exemple de publication en JSON : [https://twiki.cern.ch/twiki/pub/LCG/AccountingTaskForce/storage\_service\_v4.txt](https://twiki.cern.ch/twiki/pub/LCG/AccountingTaskForce/storage\_service\_v4.txt) 


Début de diagramme sur les critères : Dossier Critères sur la forge

fichier drawio/ sortie svg et lien sur image en ligne



prochaines visios : lundi 15/06 : 14h00

       *                     mardi 16/06 : 11h00
       *   
ordre du jour : liste des besoins utilisateurs ?  



critères les plus importants pour discuter avec groupes ontologies:  

    - ressources : short-list : cf bas du document

relations importantes pour ontologies







   * **les fournisseurs de ressources** (qui il est, qui il accepte, quels usages sont possibles)
       * quid de la fourniture de services d'hébergement pour les projets ou équipes de recherches (par ex qui ont besoin de site web avec base de données pour présenter en accès libre)?
       * Hébergement à la demande (instanciation d'un serveur Galaxy pour une communauté, etc)
       * Entrepôt/Ressources de données (NCBI)
       * []tentative de description[]
           * Nom usuel
           * Type de structure (plateau technique, UMR, mésocentre, plateforme, etc) ?
           * Principal institut de rattachement / Tutelles
           * Localisation géographique du site (Département) / lieu
           * nom+email du contact principal
           * nom+email du contact technique
           * site web
           * condition d'accès
               * gratuité ou pas / Coût
               * quels utilisateurs sont acceptés (institut, thématique, privé/publique)
                   * accessibilité localisation (carte)
               * financement
                   * ANR
                   * projet européen
               * délai d'instruction
                   * au fil de l'eau
                   * sur dossier
                       * temps xxx 
                       * tous les 2 mois
                       * ...
           * accompagnement
               * humain
               * doc
               * Support (Webpage, Manual, Tutorial)
           * URL documentation locale
           * catalogue de formation
           * Ressources: lien vers item "ressources" ci-dessous
           * certifications: ISO, Hébergeur données de santé, FitSM, CoreTrustSeal ([https://www.coretrustseal.org/about/)](https://www.coretrustseal.org/about/))
           * type de projet accepté (recherche pure, projet financé par un ministère pour diffusion auprès de professionnels)
           * Langage usité (English, ...)
           * Documents (Corporate SLA, Terms of use)
           * Service phase (Production)
           * Last Update (09.06.2020)




   * **les ressources** = les machines, logiciels, services, accompagnement/support
       * accompagnement et support (temps et thématique).
           * En partie dépendant des RH
           * lié au catalogue de formation ?                                                     
       * []tentative arborescente[]
           * datacenter 
               * localisation
               * réseau (lien WAN -débit, sécurisation, etc-)
               * disponibilité
           * ressource calcul
               * méthode d'accès
                   * cloud (IaaS: OpenStack, etc...)
                   * conteneurs (K8s, Godocker, ...)
                   * ordonnanceur batch (ou grille) / Gestionnaire de job
                   * portail web (galaxy, RStudio, Jupyterhub, ...)
               * politique d'usage
                   * durée max job
                   * nombre de job max lors de la soumission
                   * nombre de job running max par utilisateur
                   * nombre de cœurs max par job
                   * taille de VM / description des VM
                   * allocation (DARI, fairshare, réservation pour 1 période (QoS), etc...)
               * typologie de serveur physiques (ou sous-cluster)
                   * nombre de nœuds
                   * processeurs
                       * génération et modèle cf fichier modèles
                       * combien de CPU par serveur
                   * RAM
                       * capacité (UX: en Go)
                       * vitesse (UX: en MHz)
                       * affinité avec les processeurs (NUMA ou autres), doit-on prendre en compte cette info?
                   * GPU
                       * génération et modèle cf fichier modèles
                       * combien de GPU par serveur
                       * attention, comment le GPU est-il connecté dans le serveur? (doit-on prendre en compte cette info?)
                   * disque local sur les noeud de calcul
                       * taille du volume accessible en local
                   * (optionnel) localisation dans un datacenter
               * réseaux
                   * technologie 
                       * Ethernet
                       * Infiniband 
                       * OmniPath
                   * débit (UX: Gb/s), 
                   * latence (UX : ms)
                   * topologie (fat-tree, tore3D, ...)
                   * facteur de blocage
               * logiciels / services
               * Logiciels informatique, banque de données (cache local), etc
                   * nom unifié
                   * version
                   * URL (de la version ou du home, du soft ?)
                   * Description:
                       * Logiciel
                       * Banque de données (GenBank, etc.)
                       * Données météos des 50 dernières années
               * espaces de stockage (liens potentiel vers item stockage ou relation) cf "Relations entre concepts"
               * OS
                   * Distribution (Windows, CentOS, etc)
                   * version
           * ressource stockage
               * méthodes d'accès
                   * posix: NFS, GPFS, ...
                   * objet: S3, registry d'images
                   * database: SQL, NoSQL, ...
                   * entrepôt de données (NCBI, etc.): métadonnées, API, "banque de données (sans SGBD)", etc
               * partition
                   * capacité
                   * (optionnel) localisation dans un datacenter
               * "performance" (débit, latence)
               * sécurité/redondance (répliques, RAID, snapshots, sauvegardes, ...)
               * politique d'usage
                   * quota (quelle QoS: réservé ou best effort)
                   * durée (pendant combien de temps)
                   * rétention des données


           * fichiers modèles
           * Les infos pourrait être aussi automatiquement renseigné suivant les modèles (Processeur Intel Core i7-9700KF, GPU Tesla T4, ...)
               * pour décrire des types de matériels
                   * CPU
                       * nom
                       * fondeur
                       * famille
                       * modèle
                       * fréquence (UX: en GHz)
                       * nombre de cœurs
                       * niveaux de cache
                   * GPU




   * **les utilisateurs**
       *  tutelle, institut, thématique, privé/publique
       * profil (étudiant, doctorant, chercheurs, etc)
       * type de projet
       * en cas de facturation, comment obtenir un montant à intégrer dans le montage financier de son projet
       * Niveau de maîtrise ?




[]Pour réflexion[]

Pas de critères à définir mais on doit pouvoir répondre à:



   * **les besoins des utilisateurs**
       * bioinfo avec soft ?
       * machine nue + job manager
       * calcul + données
       * traitement de données : gros volume en entrée, gros volume en sortie (ou énormément de fichiers) : comment gérer cela efficacement, ainsi que la récupération des résultats
       * install perso
       * VM / conteneurs
       * données = volumes, sécurité (SSI et sauvegarde, ..)
       * volumétrie, backup …
       * Exemple: Data Stewardship Wizard ([https://ds-wizard.org)](https://ds-wizard.org)) 
       * Besoin en routine ou en coup de bourre 
       * pb de licence
       * haute disponibilité ?
       * possibilité d'éteindre une machine (gain coût si inutilisé)
       * réactivité. besoin de réponse "immédiatement"
       * projet avec un besoin ponctuel de service "rapide" (à traiter rapidement), comment gérer ce genre de demande ? serveur dédié ou priorité sur un noeud ?
       * possibilité de ZRR (zone à régime restrictif) ?
       * correspondance exacte avec les critères "ressources"?
       * Question de base à laquelle il faut apporter une réponse :
           * Pour "calcul" : j'ai un logiciel d'analyse qui tourne sur Windows, est-ce que je peux/qn peut l'installer sur un "centre de calcul" pour booster mon analyse ? (question brute de décoffrage)
           * je suis un chercheur en mécaflu, je veux coupler des calculs HPC avec une analyse IA/GPU...
           * je suis un géographe, je veux stocker de manière pérenne des fonds de cartes numériques (pour quelle durée? volume? données renouvelées régulièrement?)
           * j'ai besoin de déposer des données scientifiques quelque part et je dois les mettre à disposition du public à travers une interface web
       * Liste des besoins utilisateurs :
           * j'ai besoin de traiter de gros volumes de données (ex : télédétection) et d'avoir plein de sorties (nouvelles images)
           * j'ai besoin de données climatiques en entrée (modélisation)
           * accompagnement possible pour novice (formation, accompagnement)
           * j'ai un script R, je veux juste à avoir à le déposer (par exemple dans une page web) pour l'exécuter (besoin de formation pour ce type de besoin ?)
           * j'ai un code R-shiny sur github et j'aimerai le faire tourner avec N coeurs, M RAM, O Go de disque
           * je voudrai un docbook jupyter et j'aimerai le faire tourner avec N coeurs, M RAM, O Go de disque
           * j'ai besoin de place pour stocker des données (en sauvegarde de ce que j'ai chez moi) : quelles ressources existent? à quelles conditions?
           * J'ai récupéré un modèle d'écoulement de flux laminaire écrit en F90 et j'aimerai l'essayer avec mon jeu de données de 500Go
           * j'ai un code qui ne peut pas être lancé sur mon ordinateur, est-ce qu'il est possible de l'analyser pour connaître les besoins du programme ?
           * j'ai besoin d'avoir un niveau de service garanti pendant mon traitement (réservation de ressources : noeuds, espace de stockage)
           * j'ai une appli web qui tourne sur ma VM en local pour 5 utilisateurs (1 fichier texte en entrée, analyse via script Perl, 3 fichiers texte en sortie et présentation succincte de résultats via l'interface), où puis-je la mettre à disposition de tous en service web (n'ayant pas de compétence/support admin-sys)? Pour Cloud?




   * **recommandation des entités / tutelles** : 
       * Cloud act ?
       * Certification (Iso, Ibisa, Data Seal, RGPD, Santé)


    



[]Branches génériques qui peuvent se brancher n'importe où :[]



   * localisation
       * adresse postale
       * coordonnées GPS
   * coût
       * unité de coût
       * valeur du coût
       * politique du coût = texte
   * quota
       * unité du quota
       * valeur du quota
       * extensible sur demande (oui/non)
       * strict (oui/non)
       * politique de quota = texte
       * durée / expiration
   * péremption
       * date de décommission 
   * garantie
       * sous-garantie : oui/non 
       * date de fin de garantie : date
   * disponibilité
       * coupure
           * périodicité : annuelle, mensuelle
           * programmation
       * ondulé
       * secouru
       * climatisation redondée ?
   * réseau  / accès vers l'extérieur (au niveau nœud de calcul, frontal, etc...)
       * débit (Gbit/s)
       * entrant
           * liste des ports ouvert
           * ouverture de port de possible : oui/non
               * politique d'ouverture de port : texte
       * sortant
           * liste des ports ouvert
           * ouverture de port de possible : oui/non
               * politique d'ouverture de port : texte 


DEFINITIONS:

    

    fournisseurs de ressources : Personne physique ou morale qui fournit un ou plusieurs **services** aux utilisateurs d'un système de télécommunication (source: [http://www.marche-public.fr/Terminologie/Entrees/fournisseur-de-services.htm)](http://www.marche-public.fr/Terminologie/Entrees/fournisseur-de-services.htm))

    services : les services offerts peuvent être des machines, des logiciels,  de l'accompagnement/support. Ces services s'appuient par les moyens mis à disposition des fournisseurs de ressources.

    utilisateur : personne physique ou morale souhaitant accéder à des services 

    data center : entité physique dans lequel sont hébergés les ressources.



**Relations entre concepts (à voir avec le groupe Ontologies)**

   * Ressource calcul / datacenter
   * ressource X / fournisseur de ressources
       * multiple éventuellement. ex: cluster accessible par mesocentre et par EGI
       * fournisseur possède ou pas la ressource (cluster accessible à travers mésocentre et grille mais c'est le méso qui possède)
   * ressource stockage / datacenter
   * ressource calcul / ressource stockage
       * méthode d'accès (montage direct, etc...)


**[]Idées[]**

Groupe / Sous-groupe / partition de cluster / découpage logique / Meta-ressource ?

Exemple:

    - RODS: localisé sur plusieurs sites 

    - GRID-5000 répartit sur plusieurs sites (et chaque ressources à ses spécificités)

    - AgroDataRing (plusieurs points d'entrées)









[]Indicateurs demandés par l'IFB (extrait), pour idées[]



   * Outils / Databases / Services en ligne:
       * Nom
       * Type (Tool, DB, Both)
       * Description 
       * DOI (si plusieurs publications, séparer leurs DOI par un point-virgule)
       * Lien vers l'outil/DB
       * Conditions d'accès (Payant / gratuit)
       * Modalité de connexion (Avec / sans identification)
       * Licence (Ex: GNU, Apache, ...)
       * Première année de publication
       * Nombre de: citations totales, citations sur année N-1, téléchargements totaux, téléchargement sur année N-1, total requêtes, requêtes sur année N-1, heures CPU, heures CPU sur année N-1
       * Date de la dernière mise à jour
       * Mots clés (définis dans une liste thématique)


   * Côté administratif:
       * Appartenance à des infrastructures nationales (France Génomique, ProFi, MetaboHub, FBI, FLI, EMBRC-France, IBISBA-FR, FRISBI, PHENOMIN, Neurospin, ChemBioFrance, EMPHASIS-FR, PHENOME, CELPHEDIA, INGESTEM, E-CELL FRANCE, F-CRIN, HIDDEN, EMERG'IN, IDMIT, NEURATRIS, PGT, SOLEI, GENCI, CCIN2P3, TIMES, SOLEIL, E-RECOLNAT, ANAEE, ...)
       * Appartenance de vorre plateforme à d'autres structures du PIA (France Médecine Génomique 2025, Cohorte Constance, CAD, IHU, Labex, IRT, ...)
       * Appartenance à d'autres "European Strategy Forum on Research Infrastructures" -ESFRI- (ELIXIR, Euro-Bioimaging, INSTRUCT, INFRAFRONTIER, INFRAFRONTIER, EMPHASIS, ECRIN, ERINHA, EATRIS, ...)
       * partenaires privés


   * Et tout un tas d'autres d'indicateurs ou de données:
       * Retombées communauté scientifique: Nombre total d'usagers actifs, Nombre de nouveaux comptes utilisateur ouverts année N-1, Nombre de nouveaux espaces-projets partagés ouverts, Nombre total d'espaces-projets actifs, Nombre de nouveaux outils logiciels développés, Nombre d'utilisateurs uniques pour l'ensemble des outils logiciels web de la plateforme, Nombre de téléchargements des outils développés par la plateforme
       * Accompagnement de projets (collaboratifs ou prestations): Nombre  total de projets accompagnés, Nombre de projets concernant uniquement le laboratoire d'accueil, Nombre de projets liés à des laboratoires académiques français (hors laboratoire d'accueil), Nombre de projets avec des partenaires étrangers, Nombre de projets impliquant des partenaires privés, Pourcentage d'utilisateurs satisfaits ou très satisfaits de l'accompagnement de projets, Méthode de mesure du taux de satisfaction pour les projets accompagnés
       * Formations: Nombre de formations organisées, Nombre total de personnes formées, Nombre de formations ayant donné lieu à du matériel disponible dans des dépôts officiels, Pourcentage d'apprenants satisfaits ou très satisfaits par les formations, Méthode de mesure du taux de satisfaction pour les formations
       * Publications ...
       * Rayonnement ...
       * Taux d'utilisation: Temps d'occupation machines pour hébergement, Moyenne par usager actif, Ecart-type par usager actif
       * Valorisation: Nombre de brevets déposés, ...
       * ETP (Nombre de personnes statutaires, CDD, TR, AI, IE, IR, ... devenir des CDD ayant quitté la PF durant la période)
       * Certification: ISO-9001 ? Depuis quand ? NFX-50-900 ? Depuis quand ?
       * Tarification: grille de tarification ? Pour quel(s) type(s) d'utilisateurs? Pour quel(s) type(s) de ressources?
       * Données financières liées à la science ouverte: Montant des APC (Articles Processing Charges) payés dans le cadre du projet, Coût associés à la gestion des données du projet (stockage, gestion, mise à disposition,…)
       * Coûts liés à la gestion des données
       * + Faits marquants, CDD, Financiers, Formations, Événements organisés, publications, stages, partenariat entreprise, brevets, etc.


       * Types de services assurés sur cette infra: Analyse de données pour des usagers, Hébergement de comptes usagers (calcul et stockage), Formation, Développement de ressources logicielles (outils, DB) originales, Déploiement de ressources web (outils, DB), Autres types de service (préciser)
       * Coeurs CPU  (sans hyperthread), Lames GPU, Stockage rapide (To), Stockage capacitif (To), Stockage total, Sauvegarde sur bande ou autre type de matériel (To), Dépenses  équipement de l'année (k€), Coût directs hébergement (fluides, RH ASR, sécurité, ...) en k€, Fonctionnement hors hébergement de l'infra (k€), ETP permanents associés aux services, ETP CDD associés au service
       * Coût environné par unité de stockage (To)
       * Coût environné par unité de calcul (1000h)






==============================================================================

Proposition pour le groupe Ontologies

==============================================================================

Les items barrés sont considérés comme moins prioritaires dans une première approche



   * les ressources = les machines, logiciels, services, accompagnement/support
       * ~~accompagnement et support (temps et thématique).~~
           * ~~En partie dépendant des RH~~
           * ~~lié au catalogue de formation ?       ~~                                              
       * []tentative arborescente[]
           * datacenter 
               * localisation
               * réseau (lien WAN -débit, sécurisation, etc-)
               * ~~disponibilité~~
           * ressource calcul
               * méthode d'accès
                   * cloud (IaaS: OpenStack, etc...)
                   * ~~conteneurs (K8s, Godocker, ...)~~
                   * ordonnanceur batch (ou grille) / Gestionnaire de job
                   * ~~portail web (galaxy, RStudio, Jupyterhub, ...)~~
               * politique d'usage
                   * durée max job
                   * nombre de job max lors de la soumission
                   * nombre de job running max par utilisateur
                   * taille de VM / description des VM
                   * nombre de cœurs max par job
                   * allocation (DARI, fairshare, réservation pour 1 période (QoS), etc...)
               * typologie de serveur physiques (ou sous-cluster)
                   * nombre de nœuds
                   * processeurs
                       * génération et modèle~~ cf fichier modèles~~
                       * combien de CPU par serveur
                   * RAM
                       * capacité (UX: en Go)
                       * vitesse (UX: en MHz)
                       * ~~affinité avec les processeurs (NUMA ou autres), doit-on prendre en compte cette info?~~
                   * GPU
                       * génération et modèle ~~cf fichier modèles~~
                       * combien de GPU par serveur
                       * ~~attention, comment le GPU est-il connecté dans le serveur? (doit-on prendre en compte cette info?)~~
                   * disque local sur les noeud de calcul
                       * taille du volume accessible en local
                   * (optionnel) localisation dans un datacenter
               * réseaux
                   * technologie 
                       * Ethernet
                       * Infiniband 
                       * OmniPath
                   * débit (UX: Gb/s), 
                   * latence (UX : ms)
                   * topologie (fat-tree, tore3D, ...)
                   * facteur de blocage
               * logiciels / services
               * Logiciels informatique, banque de données (cache local), etc
                   * nom unifié
                   * version
                   * URL (de la version ou du home, du soft ?)
                   * Description:
                       * Logiciel
                       * ~~Banque de données (GenBank, etc.)~~
                       * ~~Données météos des 50 dernières années~~
               * espaces de stockage (liens potentiel vers item stockage ou relation) cf "Relations entre concepts"
               * OS
                   * Distribution (Windows, CentOS, etc)
                   * version
           * ressource stockage
               * méthodes d'accès
                   * posix: NFS, GPFS, ...
                   * ~~objet: S3, registry d'images~~
                   * ~~database: SQL, NoSQL, ...~~
                   * ~~entrepôt de données (NCBI, etc.): métadonnées, API, "banque de données (sans SGBD)", etc~~
               * partition (idem sous-cluster)
                   * capacité
                   * (optionnel) localisation dans un datacenter
               * "performance" (débit, latence)
               * sécurité/redondance (répliques, RAID, snapshots, sauvegardes, ...)
               * politique d'usage
                   * quota (quelle QoS: réservé ou best effort, i.e. surbooking)
                   * durée (pendant combien de temps)
                   * rétention des données (après fermeture de compte)


           * ~~fichiers modèles~~
           * ~~Les infos pourrait être aussi automatiquement renseigné suivant les modèles (Processeur Intel Core i7-9700KF, GPU Tesla T4, ...)~~
               * ~~pour décrire des types de matériels~~
                   * ~~CPU~~
                       * ~~nom~~
                       * ~~fondeur~~
                       * ~~famille~~
                       * ~~modèle~~
                       * ~~fréquence (UX: en GHz)~~
                       * ~~nombre de cœurs~~
                       * ~~niveaux de cache~~
                   * ~~GPU~~




Relations entre concepts (à voir avec le groupe Ontologies)

   * Ressource calcul / datacenter
   * ressource X / fournisseur de ressources
       * multiple éventuellement. ex: cluster accessible par mesocentre et par EGI
       * fournisseur possède ou pas la ressource (cluster accessible à travers mésocentre et grille mais c'est le méso qui possède)
   * ressource stockage / datacenter
   * ressource calcul / ressource stockage
       * méthode d'accès (montage direct, etc...)


** **
