# Hackathon   /   Portail Calcul-Stockage-Cloud

## Le projet au jour le jour 
  - Retrouvez la ligne de vie du projet sur le ["What's up Doc ?"](whatsupdoc.md).
  - Le [calendrier des visios](https://framagenda.org/apps/calendar/dayGridMonth/now)
  - Le [tchat mattermost](https://team.forgemia.inra.fr/hackathon-portail-calcul) et présentez vous le canal [Who's Who ?](https://team.forgemia.inra.fr/hackathon-portail-calcul/channels/whoswho)
  - Le [POC central](https://hackathon-portail-calcul.pages.mia.inra.fr/existant)
  - Les dernières [présentations](https://forgemia.inra.fr/hackathon-portail-calcul/hackathon_organisation/-/tree/master/Communication)

## Le projet  

Toutes communautés confondues, les scientifiques, chercheurs comme ITA, sont de plus en plus souvent amenés à rechercher des ressources de calcul, de stockage, de cloud... En parallèle, la diversité des offres et la complexité du monde du calcul scientifique, des différentes technologies et formes de stockage, les rendent difficiles d’accès. Il faut donc accompagner les chercheurs en simplifiant le paysage des offres et en l’accompagnant avec une documentation dédiée. 

Le projet Portail Calcul-Stockage-Cloud (PCSC) vise à créer un outil communautaire d’aide aux scientifiques. Pour ce faire, cet outil a comme but premier de les aider à trouver les ressources adaptées à leurs besoins et réellement accessibles. Au delà de répondre à la simple question “Où puis-je calculer ? Où puis-je stocker ?”, un second objectif, indispensable, est de proposer une documentation adaptée au débutant. Elle aura comme but de les aider à, entre autres, décrypter le vocabulaire idoine, spécifier leurs besoins et comprendre les enjeux de leurs choix. 

Initié par et inscrit dans le Schéma Directeur du Numérique d’INRAE, sur le volet calcul, afin d’insuffler un premier élan et aboutir à un [Proof of Concept (PoC)](https://ingenum.pages.mia.inra.fr/pcs/), le Portail Calcul-Stockage-Cloud se veut être un outil 100% utile et 100% “mutualiste” des communautés des EPST :
* utile à la communauté et à ceux qui aspirent à y accéder
* gouverné par la communauté
* construit par la communauté
* maintenu par la communauté

**Le temps est donc venu de construire ensemble notre outil !**

## Dates importantes :
**Début le lundi 25 mai 2020** avec des visios de présentation pour que chacun trouve un créneau qui lui convient  sur https://meet.jit.si/pcsc :
* 9h-10h
* 11h-12h
* 14h-15h
* 17h-18h
 
[**Semaine 1**](semaine1.md),  25 mai -29 mai 2020 : définition des objectifs

[**Semaine 2**](semaine2.md),  2 juin-5 juin 2020 : organisation des groupes de travail, recherche de compétences manquantes

[**Semaines 3 & 4**](semaines3-4.md),  8 juin-19 juin 2020 : production

[**Semaine 5**](semaine5.md),  22 juin-26 juin 2020 : mise en commun, réflexion sur la gouvernance


## Investissement personnel
L’hackathon s’étale sur 5 semaines pour être le moins intrusif possible dans les emplois du temps des participants. Le best effort est de mise et aucun engagement ferme n’est demandé.

## Fonctionnement

**Plénières tous les vendredi de 11h à 12h**

	
Outils   | utilité
---------|----------
https://meet.jit.si/pcsc | Visioconférence  (permet la connexion par téléphone contrairement au service  rendez-vous de Renater)
https://team.forgemia.inra.fr/hackathon-portail-calcul |	communication écrite entre les participants
https://forgemia.inra.fr/hackathon-portail-calcul |	Forge de l’hackathon, tout y est :  l’organisation, les tickets, le kanban, le code...
https://team.forgemia.inra.fr/hackathon-portail-calcul/channels/town-square | Chat "Ici on discute de tout"

## Inscription
1. S'authentifier une première fois sur la forge : <https://forgemia.inra.fr/hackathon-portail-calcul>
2. Envoyer un courriel d'inscription à <alexandre.dehne-garcia@inrae.fr> et <tovo.rabemanantsoa@inrae.fr>
3. Venir à une des visio du [25 mai](semaine1.md) (si ce n'est pas possible, on trouvera un autre créneau ou venez quand vous voulez les jours d'après)