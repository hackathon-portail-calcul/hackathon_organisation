# Some website references
* JSON-LD
    * https://www.w3.org/2018/jsonld-cg-reports/json-ld
    * https://json-ld.org/
* FoaF : http://xmlns.com/foaf/spec/
* W3C Standards and drafts : https://www.w3.org/TR/?tag=data#w3c_all
