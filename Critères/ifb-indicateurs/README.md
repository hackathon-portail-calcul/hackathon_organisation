
Elements demandées par l'IFB (Institut Français de Bioinforamtique) auprès des différentes plateformes/équipes.

* Les indicateurs demandés à chaque plateforme annuellement "PF_indicateurs_IFB_2020-04.xlsx". Ce fichier change chaque année me dit-on...

* L'enquete sur les besoins en "bioinfo" de l'année dernière ("Enquete_Besoins_Bioinfo_fr.pdf")

* Différentes demandes de projection ("indicateur_projection_NNCR_1.ods", "indicateur_projection_NNCR_2.ods")

