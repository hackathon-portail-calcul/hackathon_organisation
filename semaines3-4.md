# Semaines 3 et 4 - Hackathon Calcul-Stockage-Cloud
Ces 2 semaines seront consacrées au travail effectif de production au sein des groupes définis les semaines précédentes.

Chaque semaine sera clôturée par plénière de 11h à 12h en visio https://meet.jit.si/pcsc.

Ces plénières seront l'occasion de faire le point sur les avancées et les éventuels points de blocage de chaque groupe.
